# KUBERNETES
variable "cluster_name" {}
variable "rancher_version" {}
variable "k8s_version" {}
variable "rke_cli_version" {}
variable "cluster_ca" {}
variable "enable_etcd_snapshots" {}
variable "rancher_backup_interval" {type = number}
variable "rancher_backup_retention" {type = number}
variable "k8s_backup_interval" {type = number}
variable "k8s_backup_retention" {type = number}

# METADATA
#variable "env" {}
variable "env_prefix" {}
variable "project_stream" {}
variable "cloud" {}

# SCALE
variable "count_nodes" {}

# FLAVORS
variable "flavor_name" {}

# OS INFRA
variable "os_username" {}
variable "os_password" {}
variable "os_region_name" {}
variable "os_auth_url" {}
variable "os_tenant_id" {}
variable "os_domain_name" {}
variable "os_private_key" {}

# RANCHER
variable "use_rancher_private_ca" {}
variable "rancher_url" {}
variable "rancher_admin_passwd" {}
variable "rancher_cacert" {}
variable "rancher_cert" {}
variable "rancher_key" {}

# KEYCLOACK
variable "use_keycloack" {}
variable "keycloack_cert" {}
variable "keycloack_key" {}
variable "keycloack_metadata" {}

# TFSTATE COMMON
variable "use_remote_state" {}
variable "backend_path" {}
variable "backend_tf" {}

# CONSUL
variable "use_consul" {}
variable "consul_token" {}

# S3
variable "s3_service_endpoint" {}
variable "s3_bucket_name" {}
variable "s3_region" {}
variable "s3_access_key" {}
variable "s3_secret_key" {}

# STORAGE
variable "enable_etcd_ssd" {}

# MONITORING & METRICS
variable "enable_cluster_monitoring" {}
variable "vmc_endpoint_target1" {}
variable "vmc_username" {}
variable "vmc_password" {}

# NETWORK
variable "create_eip" {}
variable "az_list" {type = list(string)}
variable "image_name" {}
variable "internal_network_name" {}
variable "external_network_name" {}
variable "internal_subnet_id" {}
variable "external_subnet_id" {}
variable "create_loadbalancer" {}
variable "loadbalancer_rancher_eip" {}
variable "sg_default" {}
variable "sg_rancher" {}
variable "key_pair" {}
variable "cni" {}
variable "cluster_domain" {}
variable "cluster_cidr" {}
variable "service_cidr" {}
variable "cluster_dns_server" {}

# SSH
variable "ssh_user" {}

# PROXY
variable "http_proxy" {}
variable "https_proxy" {}
variable "no_proxy" {}

# ETCD
variable "etcd_volume_size_gb" {}

# DOCKER
variable "docker_volume_size_gb" {}
variable "docker_version" {}
variable "registry_mirror" {}
variable "docker_username" {}
variable "docker_password" {}

# SNAPSHOT RESTORE
variable "rancher_snapshot_to_restore" {}
variable "k8s_snapshot_to_restore" {}
