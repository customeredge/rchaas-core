###############################################################
###           SET RANCHER2 ADMIN PASSWORD
###############################################################

# Set Rancher2 admin password
resource "rancher2_bootstrap" "admin" {
  password          = var.rancher_admin_passwd
  telemetry         = true
  
  depends_on = [
    null_resource.rancher_scale_patch
  ]
}