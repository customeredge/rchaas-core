# Show private IPs
output "rancher_private_ips" {
    value = openstack_compute_instance_v2.instance_openstack_rancher.*.network.0.fixed_ip_v4
}

# # Cluster endpoint
# output "cluster_endpoint" {
#  value = rke_cluster.cluster.api_server_url
# }

# # Cluster certificate
# output "cluster_certificate" {
#  value = rke_cluster.cluster.ca_crt
# }

# # Client certificate
# output "client_certificate" {
#  value = rke_cluster.cluster.client_cert
# }

# # Client key
# output "client_key" {
#  value = rke_cluster.cluster.client_key
# }

# # Helm Rancher release
# output "helm_rancher_release" {
#     value = helm_release.rancher_release
# }

# Cluster authent
output "keycloak_cluster_authent" {
    value = rancher2_auth_config_keycloak.cluster_authent
}

############################################################

# # Show cluster id
# output "cluster_id" {
#     value = rke_cluster.cluster.id
# }

# Show Kubeconfig file
output "rancher_kubeconfig" {
    value = rke_cluster.cluster.kube_config_yaml
}

# Show RKE cluster yaml
output "rke_cluster_yaml" {
    value = rke_cluster.cluster.rke_cluster_yaml
}

# Show RKE cluster yaml
output "rke_state" {
    value = rke_cluster.cluster.rke_state
}

############################################################

# Rancher API Token resource
output "rancher_api_token" {
 value = rancher2_token.api_token
}

# Rancher Token Access key
output "rancher_access_key" {
 value = rancher2_token.api_token.access_key
}

# Rancher Token Secret key
output "rancher_secret_key" {
 value = rancher2_token.api_token.secret_key
}

# Rancher Token
output "rancher_token" {
 value = rancher2_token.api_token.token
}

############################################################

# ETCD snapshot after (for dependency)
output "etcd_snapshot_after" {
 value = null_resource.etcd_snapshot_after
}
