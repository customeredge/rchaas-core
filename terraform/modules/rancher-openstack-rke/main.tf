###############################################################
###                    Local Providers
###############################################################

terraform {
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "~> 1.41.0"
    }
    flexibleengine = {
      source = "FlexibleEngineCloud/flexibleengine"
      version = "~> 1.20.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "~> 2.1.0"
    }
    rke = {
      source = "rancher/rke"
      version = "~> 1.4.0"
    }
    rancher2 = {
      source = "rancher/rancher2"
      version = "~> 1.21.0"
    }
    helm = {
      source = "hashicorp/helm"
      version = "~> 2.1.2"
    }
  }
}

provider "openstack" {
  insecure = "true"

  auth_url    = var.os_auth_url
  domain_name = var.os_domain_name
  tenant_id   = var.os_tenant_id
  user_name   = var.os_username
  password    = var.os_password
  region      = var.os_region_name
}

provider "flexibleengine" {
  insecure = "true"

  auth_url    = var.os_auth_url
  domain_name = var.os_domain_name
  tenant_id   = var.os_tenant_id
  user_name   = var.os_username
  password    = var.os_password
  region      = var.os_region_name
}

provider "rancher2" {
  insecure = "true"
  bootstrap   = true
  #alias       = "bootstrap"
  api_url     = "https://${var.rancher_url}"
}

provider "rancher2" {
  alias             = "admin"
  insecure          = true
  api_url           = rancher2_bootstrap.admin.url
  token_key         = rancher2_bootstrap.admin.token
}

provider "kubernetes" {
  host                   = rke_cluster.cluster.api_server_url
  cluster_ca_certificate = rke_cluster.cluster.ca_crt
  client_certificate     = rke_cluster.cluster.client_cert
  client_key             = rke_cluster.cluster.client_key
}

provider "rke" {
  #version = "~> 1.1"
}

provider "helm" {
  kubernetes {
    host                   = rke_cluster.cluster.api_server_url
    cluster_ca_certificate = rke_cluster.cluster.ca_crt
    client_certificate     = rke_cluster.cluster.client_cert
    client_key             = rke_cluster.cluster.client_key
  }
}

###############################################################
###                        LOCALES
###############################################################

resource "local_file" "rancher_restore_file" {
    content     = templatefile("${path.module}/backups/rancher-restore.tmpl", { rancher_snapshot_to_restore = var.rancher_snapshot_to_restore })
    filename = "${path.root}/rancher-restore.yaml"
}

resource "local_file" "rancher_recurrent_backup_file" {
    content     = templatefile("${path.module}/backups/rancher-recurrent-backup.tmpl", { rancher_backup_interval = var.rancher_backup_interval, rancher_backup_retention = var.rancher_backup_retention })
    filename = "${path.root}/rancher-recurrent-backup.yaml"
}

###############################################################
###                       INSTANCES
###############################################################

# Get Image data
data "openstack_images_image_v2" "myimage" {
  name = var.image_name
  most_recent = true
}

# Create Rancher instance(s) (VMs)
resource "openstack_compute_instance_v2" "instance_openstack_rancher" {
  count = var.count_nodes
  name = "${var.env_prefix}-${var.project_stream}-${var.cloud}-${var.cluster_name}-console-${format("%02d", count.index + 1)}"
  availability_zone = var.az_list[count.index % length(var.az_list)]
  image_name = var.image_name
  flavor_name = var.flavor_name
  key_pair = var.key_pair
  security_groups = concat([var.sg_default], [var.sg_rancher])
  stop_before_destroy = "true"

  network {
    name = var.internal_network_name
  }

  block_device {
    source_type           = "image"
    uuid                  = data.openstack_images_image_v2.myimage.id
    destination_type      = "volume"
    boot_index            = 0
    volume_size           = 40
    delete_on_termination = true
  }

  block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_size           = var.docker_volume_size_gb
    boot_index            = 1
    delete_on_termination = true
  }

  /* block_device {
    source_type           = "blank"
    destination_type      = "volume"
    volume_size           = var.etcd_volume_size_gb
    boot_index            = 2
    delete_on_termination = true
    volume_type           = "SSD"
  } */

  lifecycle {
    ignore_changes = [image_name, image_id, block_device]
  }

  metadata = {
    chef_runlist = "\"recipe[DIOD-MINIMAL-SETUP::SSSD]\""
    diod_env = "DIOD-${upper(var.env_prefix)}"
    diod_enabler = var.cloud
    diod_perimeter = var.project_stream
  }
  
  # Wait 120s while chef is configuring the instance
  provisioner "local-exec"  {
    command = "sleep 150"
  }

  depends_on = [data.openstack_images_image_v2.myimage]
}

###############################################################
###          ADDITIONAL SSD DISKS/VOLUMES ON NODES
###############################################################

resource "openstack_blockstorage_volume_v3" "additional_disks_ssd_rancher" {
  count = var.enable_etcd_ssd == true || var.enable_etcd_ssd == "true" ? var.count_nodes : 0

  region                = var.os_region_name
  availability_zone     = var.az_list[count.index % length(var.az_list)]
  name                  = "${openstack_compute_instance_v2.instance_openstack_rancher[count.index].name}-etcd-ssd-disk"
  description           = "Rancher etcd ssd additional disk"
  size                  = var.etcd_volume_size_gb
  enable_online_resize  = true
  volume_type           = "SSD"

  depends_on = [openstack_compute_instance_v2.instance_openstack_rancher]
}

resource "openstack_compute_volume_attach_v2" "additional_disks_ssd_rancher_attach" {
  count = var.enable_etcd_ssd == true || var.enable_etcd_ssd == "true" ? var.count_nodes : 0

  instance_id = openstack_compute_instance_v2.instance_openstack_rancher[count.index].id
  volume_id   = openstack_blockstorage_volume_v3.additional_disks_ssd_rancher[count.index].id

  depends_on = [openstack_blockstorage_volume_v3.additional_disks_ssd_rancher]
}

###############################################################
###                   ELASTIC IP (EIP)
###############################################################

# Create Elastic/Floating IPs
resource "openstack_networking_floatingip_v2" "fip_openstack_rancher" {
  count = var.create_eip ? 3 : 0
  pool = var.external_network_name
}

# Associate Elastic/Floating IPs to instances
#resource "openstack_compute_floatingip_associate_v2" "fip_associate_openstack_rancher" {
resource "flexibleengine_compute_floatingip_associate_v2" "fip_associate_openstack_rancher" {
  count = var.create_eip ? 3 : 0

  floating_ip = openstack_networking_floatingip_v2.fip_openstack_rancher[count.index].address
  instance_id = openstack_compute_instance_v2.instance_openstack_rancher[count.index].id

  depends_on = [
    openstack_compute_instance_v2.instance_openstack_rancher,
    openstack_networking_floatingip_v2.fip_openstack_rancher
    ]
}

###############################################################
###                    LOAD BALANCER
###############################################################

resource "openstack_lb_loadbalancer_v2" "elb_openstack_rancher" {
  count         = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name          = "elb-${var.cluster_name}"
  vip_subnet_id = var.internal_subnet_id

  depends_on = [openstack_compute_instance_v2.instance_openstack_rancher]
}

resource "openstack_networking_floatingip_v2" "elb_eip_openstack_rancher" {
  count      = (var.create_loadbalancer == true || var.create_loadbalancer == "true") && var.loadbalancer_rancher_eip == null ? 1 : 0
  pool       = var.external_network_name
  port_id    = openstack_lb_loadbalancer_v2.elb_openstack_rancher[count.index].vip_port_id

  depends_on = [openstack_lb_loadbalancer_v2.elb_openstack_rancher]
}

resource "openstack_networking_floatingip_associate_v2" "elb_eip_attach_openstack_rancher" {
  count       = (var.create_loadbalancer == true || var.create_loadbalancer == "true") && var.loadbalancer_rancher_eip != null ? 1 : 0
  floating_ip = var.loadbalancer_rancher_eip
  port_id     = openstack_lb_loadbalancer_v2.elb_openstack_rancher[count.index].vip_port_id

  depends_on = [openstack_lb_loadbalancer_v2.elb_openstack_rancher]
}

resource "openstack_lb_listener_v2" "elb_listener_openstack_rancher" {
  count            = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name             = "listener-${var.cluster_name}-443"
  protocol         = "TCP"
  protocol_port    = "443"
  loadbalancer_id  = openstack_lb_loadbalancer_v2.elb_openstack_rancher[count.index].id

  depends_on = [
    openstack_networking_floatingip_v2.elb_eip_openstack_rancher,
    openstack_networking_floatingip_associate_v2.elb_eip_attach_openstack_rancher
  ]
}

resource "openstack_lb_pool_v2" "elb_pool_openstack_rancher" {
  count           = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? 1 : 0
  name            = "server_group-${var.cluster_name}"
  protocol        = "TCP"
  lb_method       = "ROUND_ROBIN"
  listener_id     = openstack_lb_listener_v2.elb_listener_openstack_rancher[count.index].id
  persistence {
    type          = "SOURCE_IP"
  }

  depends_on = [openstack_lb_listener_v2.elb_listener_openstack_rancher]
}

resource "openstack_lb_member_v2" "members" {
  count         = var.create_loadbalancer == true || var.create_loadbalancer == "true" ? var.count_nodes : 0
  name          = element(openstack_compute_instance_v2.instance_openstack_rancher.*.name, count.index)
  address       = element(openstack_compute_instance_v2.instance_openstack_rancher.*.network.0.fixed_ip_v4, count.index)
  protocol_port = "443"
  pool_id       = openstack_lb_pool_v2.elb_pool_openstack_rancher[0].id
  subnet_id     = var.internal_subnet_id

  depends_on    = [openstack_lb_pool_v2.elb_pool_openstack_rancher]
}

###############################################################
###                    NODES POST CONF
###############################################################

# Deploy Rancher RKE
resource "null_resource" "instance_openstack_rancher_postconf" {
  count = var.count_nodes
  
  triggers = {
    always_run = timestamp()
  }

  connection {
      type = "ssh"
      host = var.create_eip ? element(flexibleengine_compute_floatingip_associate_v2.fip_associate_openstack_rancher.*.floating_ip, count.index) : element(openstack_compute_instance_v2.instance_openstack_rancher.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = var.os_private_key
  }

  provisioner "file" {
    source      = "./script.sh"
    destination = "/tmp/script.sh"
  }

  provisioner "file" {
    content     = var.os_private_key
    destination = "/home/${var.ssh_user}/.ssh/id_rsa"
  }

  provisioner "file" {
     content     = var.rancher_cert
     destination = "/tmp/certs/cert.pem"
  }

  provisioner "remote-exec"  {
    inline = [
      "export http_proxy=${var.http_proxy} && export https_proxy=${var.https_proxy} && export no_proxy=${var.no_proxy}",
      "chmod +x /tmp/script.sh",
      "chmod 600 /home/${var.ssh_user}/.ssh/id_rsa",
      "[ ! -z \"${var.registry_mirror}\" ] && export REGISTRY_MIRROR=\"--registry-mirror=${var.registry_mirror}\" || export REGISTRY_MIRROR=\"\"",
      "echo \"/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --hardening-etcd --use-additional-storage=${var.docker_volume_size_gb} --use-additional-storage=${var.etcd_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --create-fs=etcd,/var/lib/etcd,${var.etcd_volume_size_gb} --user=${var.ssh_user}\"",
      "/tmp/script.sh --prerequisites --docker-version=${var.docker_version} $REGISTRY_MIRROR --hardening-all --hardening-etcd --use-additional-storage=${var.docker_volume_size_gb} --use-additional-storage=${var.etcd_volume_size_gb} --create-fs=docker,/var/lib/docker,${var.docker_volume_size_gb} --create-fs=etcd,/var/lib/etcd,${var.etcd_volume_size_gb} --user=${var.ssh_user}"
    ]
  }

  depends_on = [
    openstack_compute_instance_v2.instance_openstack_rancher,
    openstack_compute_volume_attach_v2.additional_disks_ssd_rancher_attach
  ]
}

###############################################################
###            GET TERRAFORM REMOTE STATE
###############################################################

# Get data from backend (TF state)
# CONSUL
data "terraform_remote_state" "remote_consul" {
  count = var.use_remote_state == true || var.use_remote_state == "true" ? (var.backend_tf == "consul" ? 1 : 0) : 0

  backend = var.backend_tf
  
  config = {
    path    = var.backend_path
  }

  #depends_on = [null_resource.instance_openstack_rancher_postconf]
}

# S3
data "terraform_remote_state" "remote_s3" {
  count = var.use_remote_state == true || var.use_remote_state == "true" ? (var.backend_tf == "s3" ? 1 : 0) : 0
 
  backend = var.backend_tf
  
  config = {
    bucket = var.s3_bucket_name
    key    = var.backend_path
    region = var.s3_region

    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_region_validation      = true
  }

  #depends_on = [null_resource.instance_openstack_rancher_postconf]
}

###############################################################
###            CREATE onetime ETCD SNAPSHOT
###############################################################

resource "null_resource" "etcd_snapshot_before" {
  count = (
    var.use_remote_state ? (
      var.backend_tf == "consul" ? 
        (lookup(data.terraform_remote_state.remote_consul[0].outputs, "rke_cluster_yaml", "null") != "null" ? 1 : 0) :
      var.backend_tf == "s3" ? 
        (lookup(data.terraform_remote_state.remote_s3[0].outputs, "rke_cluster_yaml", "null") != "null" ? 1 : 0) : 0
    ) : 0
  )

  triggers = {
     always_run = timestamp()
  }

  connection {
      type = "ssh"
      host = var.create_eip ? element(flexibleengine_compute_floatingip_associate_v2.fip_associate_openstack_rancher.*.floating_ip, count.index) : element(openstack_compute_instance_v2.instance_openstack_rancher.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = var.os_private_key
  }
  
  provisioner "file" {
     content     = var.backend_tf == "consul" ? data.terraform_remote_state.remote_consul[0].outputs.rke_cluster_yaml : data.terraform_remote_state.remote_s3[0].outputs.rke_cluster_yaml
     destination = "/tmp/cluster.yml"
  }

  provisioner "file" {
     content     = var.backend_tf == "consul" ? data.terraform_remote_state.remote_consul[0].outputs.rke_state : data.terraform_remote_state.remote_s3[0].outputs.rke_state
     destination = "/tmp/cluster.rkestate"
  }

  provisioner "remote-exec"  {
     inline = [
    <<EOF
    export http_proxy=${var.http_proxy} && export https_proxy=${var.https_proxy} && export no_proxy=${var.no_proxy} && \
    cd /tmp && \
    rke 2>/dev/null 1>/dev/null || \
    (curl -LO https://github.com/rancher/rke/releases/download/${var.rke_cli_version}/rke_linux-amd64 && mv ./rke_linux-amd64 ./rke && chmod +x ./rke) && \
    (./rke etcd snapshot-save --config cluster.yml --name ${timestamp()}_etcd-before --ignore-docker-version)
    EOF
    ]
  }

  depends_on = [null_resource.instance_openstack_rancher_postconf]
}

###############################################################
###               CREATE RKE CLUSTER
###############################################################

resource "rke_cluster" "cluster" {
  cluster_name = var.cluster_name
  kubernetes_version = var.k8s_version
  ssh_agent_auth = false
  ignore_docker_version = true

  # Kubernets nodes
  dynamic nodes {
    for_each = openstack_compute_instance_v2.instance_openstack_rancher
    #for_each = openstack_networking_floatingip_v2.fip_openstack_rancher
    #for_each = try(openstack_networking_floatingip_v2.fip_openstack_rancher == null ? openstack_compute_instance_v2.instance_openstack_rancher : openstack_networking_floatingip_v2.fip_openstack_rancher, openstack_networking_floatingip_v2.fip_openstack_rancher)

    content {
      address           = nodes.value.network.0.fixed_ip_v4
      #address           = nodes.value.address
      #address           = nodes.value.address == null ? nodes.value.network.0.fixed_ip_v4 : nodes.value.address
      #address           = nodes.value.address == null ? element(openstack_compute_instance_v2.instance_openstack_rancher.*.network.0.fixed_ip_v4, nodes.key) : nodes.value.address
      hostname_override = nodes.value.name
      #hostname_override = element(openstack_compute_instance_v2.instance_openstack_rancher.*.name, nodes.key)
      port              = 22
      role              = ["controlplane", "etcd", "worker"]
      user              = var.ssh_user
      ssh_key_path      = "/home/${var.ssh_user}/.ssh/id_rsa"
    }
  }

  # Network plugin
  network {
    plugin = var.cni
  }
  
  # Kubernetes services
  services {
    etcd {
      # Hardening
      gid = "52034"
      uid = "52034"
      
      # for etcd snapshots
      backup_config {
        enabled             = var.enable_etcd_snapshots
        interval_hours      = var.k8s_backup_interval
        retention           = var.k8s_backup_retention
        s3_backup_config {
          access_key        = var.s3_access_key
          secret_key        = var.s3_secret_key
          bucket_name       = var.s3_bucket_name
          #folder            = "k8s-rancher-${var.cluster_name}-etcd-backup"
          folder            = "k8s-cluster-local-etcd-backup"
          region            = var.s3_region
          endpoint          = var.s3_service_endpoint
        }
      }
      extra_env = [
        "HTTP_PROXY=${var.http_proxy}",
        "HTTPS_PROXY=${var.https_proxy}",
        "NO_PROXY=${var.no_proxy}"
      ]
    }
    kube_api {
      service_cluster_ip_range = var.service_cidr
      # Hardening
      always_pull_images = false
      pod_security_policy = true
      service_node_port_range = "30000-32767"
      secrets_encryption_config {
        enabled = true
      }
      audit_log {
        enabled = true
      }
      event_rate_limit {
        enabled = true
      }
      /* 
      # PodPresets Removed since K8S v1.20
      extra_args = {
        # Enable PodPreset
        runtime-config = "settings.k8s.io/v1alpha1=true"
        enable-admission-plugins = "PodPreset"
      } */
    }
    kube_controller {
      cluster_cidr = var.cluster_cidr
      service_cluster_ip_range = var.service_cidr
      # Hardening
      extra_args = {
        address = "127.0.0.1"
        feature-gates = "RotateKubeletServerCertificate=true"
        profiling = "false"
        terminated-pod-gc-threshold = "1000"
      }
    }
    kubelet {
      cluster_domain = var.cluster_domain
      cluster_dns_server = var.cluster_dns_server
      # Hardening
      fail_swap_on = "false"
      generate_serving_certificate = "true"
      extra_args = {
        anonymous-auth = "false"
        event-qps = "0"
        feature-gates = "RotateKubeletServerCertificate=true"
        make-iptables-util-chains = "true"
        protect-kernel-defaults = "true"
        streaming-connection-idle-timeout = "1800s"
        tls-cipher-suites = "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,TLS_RSA_WITH_AES_256_GCM_SHA384,TLS_RSA_WITH_AES_128_GCM_SHA256"
        #cloud-provider = "external"
      }
      extra_env = [
        "HTTP_PROXY=${var.http_proxy}",
        "HTTPS_PROXY=${var.https_proxy}",
        "NO_PROXY=${var.no_proxy}"
      ]
    }
    scheduler {
      # Hardening
      extra_args = {
        address = "127.0.0.1"
        profiling = "false"
      }
    }
  }

  ################################################
  # Authentication
  ################################################
  authentication {
    strategy = "x509"

    sans = []
    #  "console.di-k8s.tech.orange"
    #]
  }

  ################################################
  # Authorization
  ################################################
  authorization {
    mode = "rbac"
  }

  addons = <<EOL
---
apiVersion: v1
kind: Namespace
metadata:
  name: ingress-nginx
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: default-psp-role
  namespace: ingress-nginx
rules:
- apiGroups:
  - extensions
  resourceNames:
  - default-psp
  resources:
  - podsecuritypolicies
  verbs:
  - use
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: default-psp-rolebinding
  namespace: ingress-nginx
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: default-psp-role
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:serviceaccounts
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:authenticated
---
apiVersion: v1
kind: Namespace
metadata:
  name: cattle-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: default-psp-role
  namespace: cattle-system
rules:
- apiGroups:
  - extensions
  resourceNames:
  - default-psp
  resources:
  - podsecuritypolicies
  verbs:
  - use
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: default-psp-rolebinding
  namespace: cattle-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: default-psp-role
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:serviceaccounts
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:authenticated
---
apiVersion: policy/v1beta1
kind: PodSecurityPolicy
metadata:
  name: restricted
spec:
  requiredDropCapabilities:
  - NET_RAW
  privileged: false
  allowPrivilegeEscalation: false
  defaultAllowPrivilegeEscalation: false
  fsGroup:
    rule: RunAsAny
  runAsUser:
    rule: MustRunAsNonRoot
  seLinux:
    rule: RunAsAny
  supplementalGroups:
    rule: RunAsAny
  volumes:
  - emptyDir
  - secret
  - persistentVolumeClaim
  - downwardAPI
  - configMap
  - projected
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: psp:restricted
rules:
- apiGroups:
  - extensions
  resourceNames:
  - restricted
  resources:
  - podsecuritypolicies
  verbs:
  - use
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: psp:restricted
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: psp:restricted
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:serviceaccounts
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:authenticated
---
EOL

# apiVersion: rbac.authorization.k8s.io/v1
# kind: ClusterRoleBinding
# metadata:
#   name: rancher-webhook
# subjects:
# - kind: ServiceAccount
#   namespace: cattle-system
#   name: rancher-webhook
# roleRef:
#   apiGroup: rbac.authorization.k8s.io
#   kind: ClusterRole
#   name: cluster-admin
# ---

  # depends_on = [
  #   null_resource.instance_openstack_rancher_postconf
  # ]

  depends_on = [
    null_resource.instance_openstack_rancher_postconf,
    null_resource.etcd_snapshot_before
  ]
}

###############################################################
###                 SET KUBE_CONFIG FILE
###############################################################

resource "local_file" "kubeconfig" {
    content     = rke_cluster.cluster.kube_config_yaml
    filename = "/root/.kube/config"

    depends_on = [rke_cluster.cluster]
}

###############################################################
###              DISPLAY KUBECONFIG FILE
###############################################################

resource "null_resource" "display_kubeconfig" {
  
  triggers = {
    always_run = timestamp()
  }
  
  provisioner "local-exec"  {
    command = <<EOC
      echo "##########################################################"
      echo ""
      cat /root/.kube/config | sed "s/$/\\n/g"
      echo ""
      echo ""
      echo "##########################################################"
    EOC
  }

  depends_on = [local_file.kubeconfig]
}

###############################################################
###              DOWNLOAD KUBECTL
###############################################################

resource "null_resource" "download_kubectl" {
  
  triggers = {
    always_run = timestamp()
  }
  
  provisioner "local-exec"  {
    command = <<EOC
    kubectl 2>/dev/null 1>/dev/null || \
    (while ! apk --no-cache add curl 2>/dev/null;do echo "Waiting apk available (sleeping 5s)..." && sleep 5; done && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && mv ./kubectl /usr/local/bin && chmod +x /usr/local/bin/kubectl)
    EOC
  }

  depends_on = [local_file.kubeconfig]
}

###############################################################
###            PATCH INGRESS-NGINX proxy-body-size
###############################################################

resource "null_resource" "patch_ingress" {
  /* triggers = {
     always_run = timestamp()
  } */

  provisioner "local-exec"  {
    command = <<EOC
    kubectl -n ingress-nginx patch configmap nginx-configuration -p '{"data":{"proxy-body-size":"0"}}'
    EOC
  }

  depends_on = [
    local_file.kubeconfig,
    null_resource.download_kubectl
  ]
}

###############################################################
###                 GET CLUSTER YAML
###############################################################

# resource "local_file" "rke_cluster_yaml" {
#   #count = var.restore_snapshot ? 1 : 0

#   content = rke_cluster.cluster.rke_cluster_yaml
#   filename = "${path.root}/rke_data/cluster.yml"

#   depends_on = [rke_cluster.cluster]
# }

###############################################################
###                 GET RKE STATE
###############################################################

# resource "local_file" "rke_state" {
#   #count = var.restore_snapshot ? 1 : 0

#   content = rke_cluster.cluster.rke_state
#   filename = "${path.root}/rke_data/cluster.rkestate"

#   depends_on = [rke_cluster.cluster]
# }

###############################################################################
# If you need ca_crt/client_cert/client_key, please uncomment follows.
###############################################################################
# resource "local_file" "ca_crt" {
#  filename = "${path.root}/ca_cert"
#  content  = rke_cluster.cluster.ca_crt
# }

# resource "local_file" "client_cert" {
#  filename = "${path.root}/client_cert"
#  content  = rke_cluster.cluster.client_cert
# }

# resource "local_file" "client_key" {
#  filename = "${path.root}/client_key"
#  content  = rke_cluster.cluster.client_key
# }

###############################################################
###                    CONSUL REGISTER
###############################################################

# Register console consul service
resource "null_resource" "consul_register_service" {
  count = var.use_consul == "true" ? var.count_nodes : 0

  triggers = {
     always_run = timestamp()
  }

  connection {
      type = "ssh"
      host = var.create_eip ? element(flexibleengine_compute_floatingip_associate_v2.fip_associate_openstack_rancher.*.floating_ip, count.index) : element(openstack_compute_instance_v2.instance_openstack_rancher.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = var.os_private_key
  }

  provisioner "remote-exec"  {
    inline = [
    <<EOF
cat <<EOT | sudo tee /opt/application/Consul/conf/service-registration.json
{
  "services": [
    {
      "token": "${var.consul_token}",
      "name": "${var.env_prefix}-${var.project_stream}-console",
      "Address": "${element(openstack_compute_instance_v2.instance_openstack_rancher.*.network.0.fixed_ip_v4, count.index)}",
      "Port": 443,
      "tags": [
        "gin.enable=true"
      ],
      "Checks": [
        {
          "Name" : "Rancher health check",
          "status": "passing",
          "http": "https://localhost/healthz",
          "tls_skip_verify": true,
          "method": "GET",
          "Interval": "5s",
          "timeout": "2s"
        }
      ]
    }
  ]
}
EOT
/usr/bin/sudo chown consul:consul /opt/application/Consul/conf/service-registration.json
/usr/bin/sudo systemctl daemon-reload
/usr/bin/sudo systemctl reload consul
    EOF
    ]

       # "gin.tcp.routers.${var.env_prefix}-${var.project_stream}.entrypoints=websecure",
       # "gin.tcp.routers.${var.env_prefix}-${var.project_stream}.rule=HostSNI('${var.rancher_url}')",
       # "gin.tcp.routers.${var.env_prefix}-${var.project_stream}.service=${var.env_prefix}-${var.project_stream}-console",
       # "gin.tcp.routers.${var.env_prefix}-${var.project_stream}.tls=true",
       # "gin.tcp.routers.${var.env_prefix}-${var.project_stream}.tls.passthrough=true",
       # "gin.tcp.routers.${var.env_prefix}-${var.project_stream}.tls.options=default"
  }

  depends_on = [rke_cluster.cluster]
}

###############################################################
###                 RESTORE ETCD SNAPSHOT
###############################################################

# Restore ETCD snapshot
resource "null_resource" "etcd_snapshot_restore" {
  count = var.k8s_snapshot_to_restore != "" ? 1 : 0
  
  connection {
      type = "ssh"
      host = var.create_eip ? element(flexibleengine_compute_floatingip_associate_v2.fip_associate_openstack_rancher.*.floating_ip, count.index) : element(openstack_compute_instance_v2.instance_openstack_rancher.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = var.os_private_key
  }
  
  provisioner "file" {
     content     = rke_cluster.cluster.rke_cluster_yaml
     destination = "/tmp/cluster.yml"
  }

  # provisioner "file" {
  #   content     = rke_cluster.cluster.rke_state
  #   destination = "/tmp/cluster.rkestate"
  #} 

  provisioner "remote-exec"  {
     inline = [
    <<EOF
    export http_proxy=${var.http_proxy} && export https_proxy=${var.https_proxy} && export no_proxy=${var.no_proxy} && \
    cd /tmp && \
    rke 2>/dev/null 1>/dev/null || \
    (curl -LO https://github.com/rancher/rke/releases/download/${var.rke_cli_version}/rke_linux-amd64 && mv ./rke_linux-amd64 ./rke && chmod +x ./rke) && \
    (./rke etcd snapshot-restore --config cluster.yml --name ${var.k8s_snapshot_to_restore} --ignore-docker-version)
    EOF
    ]
  }

  depends_on = [
    #null_resource.etcd_snapshot_before,
    #null_resource.add_proxy_rancher_backup,
    rke_cluster.cluster
  ]
}

###############################################################
###                  HELM RANCHER BACKUP
###############################################################

# Create a S3 credentials secret
resource "kubernetes_secret" "s3_credentials_secret" {
  
  metadata {
    name = "s3-credentials"
    namespace = "cattle-system"
  }

  data = {
    "accessKey" = var.s3_access_key
    "secretKey" = var.s3_secret_key
  }

  type      = "Opaque"

  depends_on = [
    rke_cluster.cluster,
    #rancher2_auth_config_keycloak.cluster_authent
  ]
  #depends_on = [null_resource.etcd_snapshot_after]
}

resource "helm_release" "rancher_backup_crd" {
  name              = "rancher-backup-crd"
  repository        = "https://charts.rancher.io"
  chart             = "rancher-backup-crd"
  namespace         = "cattle-resources-system"
  #timeout           = "600"
  create_namespace  = true

  depends_on = [kubernetes_secret.s3_credentials_secret]
}

resource "helm_release" "rancher_backup" {
  name              = "rancher-backup"
  repository        = "https://charts.rancher.io"
  chart             = "rancher-backup"
  namespace         = "cattle-resources-system"
  #timeout           = "600"
  
  set {
    name  = "s3.enabled"
    value = true
  }

  set {
    name  = "s3.credentialSecretName"
    value = "s3-credentials"
  }

  set {
    name  = "s3.credentialSecretNamespace"
    value = "cattle-system"
  }

  set {
    name  = "s3.region"
    value = var.s3_region
  }

  set {
    name  = "s3.bucketName"
    value = var.s3_bucket_name
  }

  set {
    name  = "s3.folder"
    value = "k8s-rancher-${var.cluster_name}-etcd-backup"
  }

  set {
    name  = "s3.endpoint"
    value = var.s3_service_endpoint
  }

  set {
    name  = "s3.insecureTLSSkipVerify"
    value = true
  }

  depends_on = [helm_release.rancher_backup_crd]
}

###############################################################
###              ADD PROXY TO RANCHER BACKUP
###############################################################

resource "null_resource" "add_proxy_rancher_backup" {
  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec"  {
    command = <<EOC
    kubectl -n cattle-resources-system set env deployment/rancher-backup HTTP_PROXY=${var.http_proxy} && \
    kubectl -n cattle-resources-system set env deployment/rancher-backup HTTPS_PROXY=${var.https_proxy} && \
    kubectl -n cattle-resources-system set env deployment/rancher-backup NO_PROXY=${var.no_proxy}
    EOC
  }

  depends_on = [
    local_file.kubeconfig,
    null_resource.download_kubectl,
    helm_release.rancher_backup
  ]
}

###############################################################
###               RESTORE RANCHER BACKUP
###############################################################

resource "null_resource" "restore_rancher_backup" {
  count = var.rancher_snapshot_to_restore != "" ? 1 : 0

  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec"  {
    command = "kubectl apply -f ${path.root}/rancher-restore.yaml"
  }

  depends_on = [
    local_file.kubeconfig,
    null_resource.download_kubectl,
    null_resource.add_proxy_rancher_backup
  ]
}

###############################################################
###                 CREATE RANCHER CACERT
###############################################################

# Create additional CA tls-ca-additional secret
resource "kubernetes_secret" "rancher_additional_ca" {
  count = var.use_rancher_private_ca == "false" ? 1 : 0

  metadata {
    name = "tls-ca-additional"
    namespace = "cattle-system"
  }

  data = {
    "ca-additional.pem" = var.rancher_cacert
  }

  type      = "Opaque"

  #depends_on = [null_resource.consul_register_service]
  #depends_on = [rke_cluster.cluster]
  depends_on = [
    rke_cluster.cluster,
    null_resource.add_proxy_rancher_backup
    #null_resource.etcd_snapshot_restore
  ]
}

# Create the tls-ca secret
resource "kubernetes_secret" "rancher_cacert" {
  count = var.use_rancher_private_ca == "true" ? 1 : 0

  metadata {
    name = "tls-ca"
    namespace = "cattle-system"
  }

  data = {
    "cacerts.pem" = var.rancher_cacert
  }

  type      = "Opaque"

  depends_on = [kubernetes_secret.rancher_additional_ca]
}

# Create the tls-rancher-ingress secret
resource "kubernetes_secret" "rancher_tls" {
  count = var.use_rancher_private_ca == "true" ? 1 : 0

  metadata {
    name = "tls-rancher-ingress"
    namespace = "cattle-system"
  }

  data = {
    "tls.crt" = var.rancher_cert
    "tls.key" = var.rancher_key
  }

  type      = "kubernetes.io/tls"

  depends_on = [kubernetes_secret.rancher_cacert]
}

###############################################################
###                  HELM CERT MANAGER
###############################################################

resource "helm_release" "certmanager_release" {
  count = var.use_rancher_private_ca == "false" || var.rancher_snapshot_to_restore != "" ? 1 : 0

  name              = "cert-manager"
  repository        = "https://charts.jetstack.io"
  chart             = "cert-manager"
  namespace         = "cert-manager"
  create_namespace  = true
  timeout           = "600"

  set {
    name  = "installCRDs"
    value = true
  }

  #depends_on = [null_resource.consul_register_service]
  depends_on = [
    null_resource.restore_rancher_backup,
    kubernetes_secret.rancher_additional_ca,
    kubernetes_secret.rancher_cacert,
    kubernetes_secret.rancher_tls
  ]
}

###############################################################
###                  HELM RANCHER INSTALL
###############################################################

resource "helm_release" "rancher_release_letsencrypt" {
  # Need to be improved
  count = 0
  
  name              = "rancher"
  repository        = "https://releases.rancher.com/server-charts/latest"
  chart             = "rancher"
  version           = var.rancher_version
  namespace         = "cattle-system"
  timeout           = "600"
  #create_namespace  = true

  set {
    name  = "hostname"
    value = var.rancher_url
  }

  set {
    name  = "proxy"
    value = var.https_proxy
  }

  set {
    name  = "ingress.tls.source"
    value = "letsEncrypt"
  }

  set {
    name  = "letsEncrypt.email"
    value = "diod.k8s@orange.com"
  }

  set {
    name  = "bootstrapPassword"
    value = var.rancher_admin_passwd
  }

  set {
    name  = "auditLog.level"
    value = 1
  }

  depends_on = [
    helm_release.certmanager_release
    #null_resource.certmanager_wait
  ]
}

resource "helm_release" "rancher_release_selfsigned" {
  count = var.use_rancher_private_ca == "false" ? 1 : 0
  
  name              = "rancher"
  repository        = "https://releases.rancher.com/server-charts/latest"
  chart             = "rancher"
  version           = var.rancher_version
  namespace         = "cattle-system"
  timeout           = "600"
  #force_update      = true
  wait_for_jobs     = true
  #create_namespace  = true

  set {
    name  = "hostname"
    value = var.rancher_url
  }

  set {
    name  = "proxy"
    value = var.https_proxy
  }

  set {
    name = "tls"
    value = "external"
  }

  set {
    name = "additionalTrustedCAs"
    value = "true"
  }

  set {
    name  = "bootstrapPassword"
    value = var.rancher_admin_passwd
  }

  set {
    name  = "auditLog.level"
    value = 1
  }

  depends_on = [
    helm_release.certmanager_release
    #null_resource.certmanager_wait
  ]
}

resource "helm_release" "rancher_release_privateca" {
  count = var.use_rancher_private_ca == "true" ? 1 : 0
  
  name              = "rancher"
  repository        = "https://releases.rancher.com/server-charts/latest"
  chart             = "rancher"
  version           = var.rancher_version
  namespace         = "cattle-system"
  timeout           = "600"
  #create_namespace  = true

  set {
    name  = "hostname"
    value = var.rancher_url
  }

  set {
    name  = "proxy"
    value = var.https_proxy
  }

  set {
    name  = "ingress.tls.source"
    value = "secret"
  }

  set {
    name  = "privateCA"
    value = true
  }

  set {
    name  = "bootstrapPassword"
    value = var.rancher_admin_passwd
  }

  set {
    name  = "auditLog.level"
    value = 1
  }

  depends_on = [
    helm_release.certmanager_release
    #null_resource.certmanager_wait
  ]
}

###############################################################
###              RANCHER SCALE PATCH
###############################################################

resource "null_resource" "rancher_scale_patch" {

  provisioner "local-exec"  {
    command = <<EOC
    kubectl -n cattle-system scale deployment rancher --replicas=3 && \
    while ! kubectl -n cattle-system get deployment rancher | grep 3/3 1>/dev/null 2>/dev/null; do echo "Waiting rancher to be available (sleeping 5s)..." && sleep 5; done
    EOC
  }

  depends_on = [
    local_file.kubeconfig,
    null_resource.download_kubectl,
    helm_release.rancher_release_letsencrypt,
    helm_release.rancher_release_selfsigned,
    helm_release.rancher_release_privateca
  ]
}

###############################################################
###               SET RANCHER2 URL
###############################################################

# Set Rancher2 server-url
resource "rancher2_setting" "url" {
  provider = rancher2.admin
  name = "server-url"
  value = "https://${var.rancher_url}"

  # depends_on = [
  #   helm_release.rancher_release_letsencrypt,
  #   helm_release.rancher_release_selfsigned,
  #   helm_release.rancher_release_privateca
  # ]
}

###############################################################
###               CREATE RANCHER TOKEN (API)
###############################################################

# Create a new rancher2 Token
resource "rancher2_token" "api_token" {
  provider = rancher2.admin
  description = "K8SaaS API token"
  #ttl = 1200

  # depends_on = [rancher2_setting.url]
}

###############################################################
###               CLUSTER AUTHENTICATION
###############################################################

# Create a new rancher2 Auth Config KeyCloak
resource "rancher2_auth_config_keycloak" "cluster_authent" {
  count = var.use_keycloack == "true" ? 1 : 0
  provider = rancher2.admin

  display_name_field = "name"
  groups_field = "uid"
  idp_metadata_content = var.keycloack_metadata
  rancher_api_host = "https://${var.rancher_url}"
  sp_cert = var.keycloack_cert
  sp_key = var.keycloack_key
  uid_field = "uid"
  user_name_field = "uid"

  depends_on = [rancher2_setting.url]
}

###############################################################
###                    GET SYSTEM DATA
###############################################################

# Get System project data
data "rancher2_project" "project_system" {
  provider = rancher2.admin

  name = "System"
  cluster_id = "local"

  depends_on = [
    rke_cluster.cluster,
    rancher2_setting.url
  ]
}

###############################################################
###               CLUSTER MONITORING
###############################################################

# Create cattle-monitoring-system namespace
resource "rancher2_namespace" "namespace_monitoring" {
  count = var.enable_cluster_monitoring == true || var.enable_cluster_monitoring == "true" ? 1 : 0
  provider = rancher2.admin

  name = "cattle-monitoring-system"
  project_id = data.rancher2_project.project_system.id

  depends_on = [data.rancher2_project.project_system]
}

# Create Victoria Metrics Secret
resource "rancher2_secret" "vmc_creds" {
  count = var.enable_cluster_monitoring == true || var.enable_cluster_monitoring == "true" ? 1 : 0
  provider = rancher2.admin

  name = "vmc-creds"
  description = "Victoria Metrics Secret"
  project_id = data.rancher2_project.project_system.id
  namespace_id = rancher2_namespace.namespace_monitoring[count.index].id
  data = {
    username = base64encode(var.vmc_username)
    password = base64encode(var.vmc_password)
  }

  lifecycle {
    ignore_changes = [labels]
  }

  depends_on = [rancher2_namespace.namespace_monitoring]
}

# Create Victoria Metrics Certs Secret
resource "rancher2_secret" "vmc_certs" {
  count = var.enable_cluster_monitoring == true || var.enable_cluster_monitoring == "true" ? 1 : 0
  provider = rancher2.admin

  name = "vmc-certs"
  description = "Victoria Metrics Certs"
  project_id = data.rancher2_project.project_system.id
  namespace_id = rancher2_namespace.namespace_monitoring[count.index].id
  data = {
    "ca.crt" = base64encode(var.cluster_ca)
  }

  lifecycle {
    ignore_changes = [labels]
  }

  depends_on = [rancher2_namespace.namespace_monitoring]
}

# Deploy cluster monitoring app
resource "rancher2_app_v2" "cluster_monitoring" {
  count = var.enable_cluster_monitoring == true || var.enable_cluster_monitoring == "true" ? 1 : 0
  provider = rancher2.admin

  cluster_id = "local"
  name = "rancher-monitoring"
  namespace = "cattle-monitoring-system"
  repo_name = "rancher-charts"
  chart_name = "rancher-monitoring"
  chart_version = "14.5.100"
  #values = file("values.yaml")
  values = <<EOF
prometheus:
  prometheusSpec:
    resources:
      limits:
        cpu: 1000m
        memory: 4000Mi
      requests:
        cpu: 750m
        memory: 2000Mi
    externalLabels: 
      cluster: ${var.cluster_name}
    remoteWrite:
      - basicAuth:
          password:
            key: password
            name: vmc-creds
          username:
            key: username
            name: vmc-creds
        queueConfig:
          maxShards: 5
        tlsConfig:
          ca:
            secret:
              name: vmc-certs
              key: ca.crt
          insecureSkipVerify: false
        url: '${var.vmc_endpoint_target1}'
EOF  

  depends_on = [
    rancher2_secret.vmc_creds,
    rancher2_secret.vmc_certs
  ]
}

###############################################################
###              ADD PROXY TO CATTLE SYSTEM
###############################################################

#
## not needed for Rancher v2.5
#

# resource "null_resource" "add_proxy" {
# count = 0
#   /* triggers = {
#      always_run = timestamp()
#   } */

#   provisioner "local-exec"  {
#     command = <<EOC
#     sleep 180 && \
#     kubectl 2>/dev/null 1>/dev/null || \
#     (apk --no-cache add curl && \
#     curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && mv ./kubectl /usr/local/bin && chmod +x /usr/local/bin/kubectl) && \
#     (kubectl -n cattle-system set env deployment/cattle-cluster-agent HTTP_PROXY=${var.http_proxy} && \
#     kubectl -n cattle-system set env deployment/cattle-cluster-agent HTTPS_PROXY=${var.https_proxy} && \
#     kubectl -n cattle-system set env deployment/cattle-cluster-agent NO_PROXY=${var.no_proxy} && \
#     kubectl -n cattle-system set env DaemonSet/cattle-node-agent HTTP_PROXY=${var.http_proxy} && \
#     kubectl -n cattle-system set env DaemonSet/cattle-node-agent HTTPS_PROXY=${var.https_proxy} && \
#     kubectl -n cattle-system set env DaemonSet/cattle-node-agent NO_PROXY=${var.no_proxy})
#     EOC
#   }

#   depends_on = [local_file.kubeconfig]
# }


###############################################################
###            CREATE RANCHER RECURRENT BACKUP
###############################################################

# Create Rancher recurrent backup
resource "null_resource" "create_rancher_recurrent_backup" {
  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec"  {
    command = "kubectl -n cattle-resources-system apply -f ${path.root}/rancher-recurrent-backup.yaml"
  }

  depends_on = [
    local_file.kubeconfig,
    null_resource.download_kubectl,
    rancher2_setting.url,
    null_resource.add_proxy_rancher_backup
  ]
}

###############################################################
###              ADD PROXY TO FLEET
###############################################################

resource "null_resource" "add_proxy_fleet" {
  triggers = {
    always_run = timestamp()
  }

  provisioner "local-exec"  {
    command = <<EOC
    { { kubectl get namespace cattle-fleet-system 2>/dev/null 1>/dev/null && export NS=cattle-fleet-system; } || \
    { kubectl get namespace fleet-system 2>/dev/null 1>/dev/null && export NS=fleet-system; }; } && \
    while ! kubectl -n $NS get deployment/fleet-controller 2>/dev/null;do echo "Waiting fleet-controller (sleeping 5s)..." && sleep 5; done && \
    ( kubectl -n $NS set env deployment/fleet-controller HTTP_PROXY=${var.http_proxy} && \
      kubectl -n $NS set env deployment/fleet-controller HTTPS_PROXY=${var.https_proxy} && \
      kubectl -n $NS set env deployment/fleet-controller NO_PROXY=${var.no_proxy} && \
      kubectl -n $NS set env deployment/fleet-agent HTTP_PROXY=${var.http_proxy} && \
      kubectl -n $NS set env deployment/fleet-agent HTTPS_PROXY=${var.https_proxy} && \
      kubectl -n $NS set env deployment/fleet-agent NO_PROXY=${var.no_proxy} ) || true
    EOC
  }

  depends_on = [
    local_file.kubeconfig,
    null_resource.download_kubectl,
    null_resource.create_rancher_recurrent_backup
  ]
}

###############################################################
###            CREATE onetime ETCD SNAPSHOT
###############################################################

resource "null_resource" "etcd_snapshot_after" {
  count = 1

  triggers = {
     always_run = timestamp()
  }

  connection {
      type = "ssh"
      host = var.create_eip ? element(flexibleengine_compute_floatingip_associate_v2.fip_associate_openstack_rancher.*.floating_ip, count.index) : element(openstack_compute_instance_v2.instance_openstack_rancher.*.network.0.fixed_ip_v4, count.index)
      user = var.ssh_user
      private_key = var.os_private_key
  }
  
  provisioner "file" {
     content     = rke_cluster.cluster.rke_cluster_yaml
     destination = "/tmp/cluster.yml"
  }

  provisioner "file" {
     content     = rke_cluster.cluster.rke_state
     destination = "/tmp/cluster.rkestate"
  }

  provisioner "remote-exec"  {
     inline = [
    <<EOF
    export http_proxy=${var.http_proxy} && export https_proxy=${var.https_proxy} && export no_proxy=${var.no_proxy} && \
    cd /tmp && \
    rke 2>/dev/null 1>/dev/null || \
    (curl -LO https://github.com/rancher/rke/releases/download/${var.rke_cli_version}/rke_linux-amd64 && mv ./rke_linux-amd64 ./rke && chmod +x ./rke) && \
    (./rke etcd snapshot-save --config cluster.yml --name ${timestamp()}_etcd-after --ignore-docker-version)
    EOF
    ]
  }

  depends_on = [
    null_resource.instance_openstack_rancher_postconf,
    null_resource.etcd_snapshot_before,
    rke_cluster.cluster,
    helm_release.rancher_release_letsencrypt,
    helm_release.rancher_release_selfsigned,
    helm_release.rancher_release_privateca,
    #null_resource.etcd_snapshot_restore,
    null_resource.create_rancher_recurrent_backup,
    null_resource.add_proxy_fleet,
    rancher2_app_v2.cluster_monitoring
  ]
}

###############################################################
