###############################################################
###                   Common Providers
###############################################################

provider "null" {
  version = "~> 2.1"
}

provider "local" {
  version = "~> 2.0"
}

###############################################################
###                      Script Version
###############################################################

resource "null_resource" "display_script_version" {
  triggers = {
    always_run = timestamp()
  }
    
  provisioner "local-exec"  {
      command = <<EOC
      echo ""
      echo ""
      echo "############################################################################################"
      echo "################################### SCRIPT VERSION: ${local.script_version} ##################################"
      echo "############################################################################################"
      echo ""
      echo ""
      EOC
  }
}

###############################################################
###                      Modules
###############################################################

# Execute k8s-cluster module
module "rancher_openstack_rke" {
  source = "./modules/rancher-openstack-rke"
  
  # KUBERNETES
  cluster_name                = local.json_data.cluster_name
  rancher_version             = local.json_data.parameters.cluster.client.rancher_version
  k8s_version                 = local.json_data.parameters.cluster.client.k8s_version
  rke_cli_version             = local.json_data.parameters.cluster.client.rke_cli_version
  enable_etcd_snapshots       = local.json_data.parameters.cluster.admin.enable_etcd_snapshots
  rancher_backup_interval     = local.json_data.parameters.cluster.admin.rancher_backup_interval
  rancher_backup_retention    = local.json_data.parameters.cluster.admin.rancher_backup_retention
  k8s_backup_interval         = local.json_data.parameters.cluster.admin.k8s_backup_interval
  k8s_backup_retention        = local.json_data.parameters.cluster.admin.k8s_backup_retention
  cluster_ca                  = local.cluster_ca
  
  # METADATA
  env_prefix                  = local.json_data.parameters.cluster.client.env_prefix
  project_stream              = local.json_data.parameters.cluster.admin.project_stream
  cloud                       = local.cloud
  
  # SCALE 
  count_nodes                 = local.json_data.parameters.cluster.admin.count_nodes

  # IMAGE
  image_name                  = local.json_data.parameters.cluster.admin.image_name
  
  # FLAVORS
  flavor_name                 = local.json_data.parameters.cluster.admin.flavor_name

  # TFSTATE COMMON
  use_remote_state            = local.json_data.parameters.cluster.admin.use_remote_state
  backend_tf                  = local.json_data.parameters.cluster.admin.backend_tf
  backend_path                = var.backend_path

  # CONSUL
  use_consul                  = local.json_data.parameters.cluster.admin.use_consul
  consul_token                = local.consul_token

  # S3
  s3_service_endpoint         = local.s3_service_endpoint
  s3_bucket_name              = local.s3_bucket_name
  s3_region                   = local.s3_region
  s3_access_key               = local.s3_access_key
  s3_secret_key               = local.s3_secret_key

  # STORAGE
  enable_etcd_ssd             = local.enable_etcd_ssd

  # MONITORING & METRICS
  enable_cluster_monitoring   = local.json_data.parameters.cluster.admin.enable_cluster_monitoring
  vmc_endpoint_target1        = local.vmc_endpoint_target1
  vmc_username                = local.vmc_username
  vmc_password                = local.vmc_password

  # NETWORK
  az_list                     = split(",", local.az_list)
  internal_network_name       = local.internal_network_name
  external_network_name       = local.external_network_name
  internal_subnet_id          = local.internal_subnet_id
  external_subnet_id          = local.external_subnet_id
  create_loadbalancer         = local.json_data.parameters.cluster.admin.create_loadbalancer
  loadbalancer_rancher_eip    = local.loadbalancer_rancher_eip
  sg_default                  = local.sg_default
  sg_rancher                  = local.sg_rancher
  cni                         = local.json_data.parameters.cluster.admin.cni
  cluster_domain              = local.json_data.parameters.cluster.admin.cluster_domain
  cluster_cidr                = local.json_data.parameters.cluster.admin.cluster_cidr
  service_cidr                = local.json_data.parameters.cluster.admin.service_cidr
  cluster_dns_server          = local.json_data.parameters.cluster.admin.cluster_dns_server
  create_eip                  = local.json_data.parameters.cluster.admin.create_eip
  
  # SSH
  ssh_user                    = local.json_data.parameters.cluster.admin.ssh_user
  key_pair                    = local.key_pair

  # PROXY
  http_proxy                  = local.json_data.parameters.cluster.admin.http_proxy
  https_proxy                 = local.json_data.parameters.cluster.admin.https_proxy
  no_proxy                    = local.json_data.parameters.cluster.admin.no_proxy

  # ETCD
  etcd_volume_size_gb         = local.json_data.parameters.cluster.admin.etcd_volume_size_gb

  # DOCKER
  docker_volume_size_gb       = local.json_data.parameters.cluster.admin.docker_volume_size_gb
  docker_version              = local.json_data.parameters.cluster.client.docker_version
  registry_mirror             = local.json_data.parameters.cluster.admin.registry_mirror
  docker_username             = local.docker_username
  docker_password             = local.docker_password

  # OPENSTACK
  os_domain_name              = local.os_domain_name
  os_auth_url                 = local.os_auth_url
  os_region_name              = local.os_region_name
  os_tenant_id                = local.os_tenant_id
  os_username                 = local.os_username
  os_password                 = local.os_password
  os_private_key              = local.os_private_key
  
  # RANCHER
  use_rancher_private_ca      = local.json_data.parameters.cluster.admin.use_rancher_private_ca
  rancher_url                 = local.json_data.parameters.cluster.client.rancher_url
  rancher_admin_passwd        = local.json_data.parameters.cluster.client.rancher_admin_passwd
  rancher_cacert              = local.rancher_cacert
  rancher_cert                = local.rancher_cert
  rancher_key                 = local.rancher_key
 
  # KEYCLOACK
  use_keycloack               = local.json_data.parameters.cluster.admin.use_keycloack
  keycloack_cert              = local.keycloack_cert
  keycloack_key               = local.keycloack_key
  keycloack_metadata          = local.keycloack_metadata

  # SNAPSHOT RESTORE
  rancher_snapshot_to_restore = local.json_data.parameters.cluster.client.rancher_snapshot_to_restore
  k8s_snapshot_to_restore     = local.json_data.parameters.cluster.client.k8s_snapshot_to_restore
}

###############################################################
