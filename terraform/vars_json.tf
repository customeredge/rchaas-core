###############################################################
###                   Get json parameters
###############################################################

locals {
  script_version            = "0.2.0"

  json_data = jsondecode(file(var.parameters))

  #env_prefix                = local.json_data.parameters.cluster.client.env_prefix
  #env_prefix_dns            = local.env_prefix == "pr" ? "" : "${local.env_prefix}-"
  
  os_domain_name            = local.json_data.parameters.infrastructure.cloud.OS_DOMAIN_NAME
  os_auth_url               = local.json_data.parameters.infrastructure.cloud.OS_AUTH_URL
  os_region_name            = local.json_data.parameters.infrastructure.cloud.OS_REGION_NAME
  os_tenant_id              = local.json_data.parameters.infrastructure.cloud.OS_TENANT_ID
  os_username               = local.json_data.parameters.infrastructure.cloud.OS_USERNAME
  os_password               = local.json_data.parameters.infrastructure.cloud.OS_PASSWORD
  os_private_key            = local.json_data.parameters.infrastructure.cloud.OS_PRIVATE_KEY
  cluster_ca                = local.json_data.parameters.infrastructure.cloud.CLUSTER_CA
  az_list                   = local.json_data.parameters.infrastructure.cloud.az_list
  sg_default                = local.json_data.parameters.infrastructure.cloud.sg_default
  sg_rancher                = local.json_data.parameters.infrastructure.cloud.sg_rancher
  cloud                     = local.json_data.parameters.infrastructure.cloud.cloud
  key_pair                  = local.json_data.parameters.infrastructure.cloud.key_pair
  internal_network_name     = local.json_data.parameters.infrastructure.cloud.internal_network_name
  external_network_name     = local.json_data.parameters.infrastructure.cloud.external_network_name
  internal_subnet_id        = local.json_data.parameters.infrastructure.cloud.internal_subnet_id
  external_subnet_id        = local.json_data.parameters.infrastructure.cloud.external_subnet_id
  loadbalancer_rancher_eip  = local.json_data.parameters.infrastructure.cloud.loadbalancer_rancher_eip
  
  rancher_cacert            = local.json_data.parameters.infrastructure.rancher.CACERT
  rancher_cert              = local.json_data.parameters.infrastructure.rancher.CERT
  rancher_key               = local.json_data.parameters.infrastructure.rancher.KEY

  enable_etcd_ssd           = lookup(local.json_data.parameters.cluster.admin,"enable_etcd_ssd","null") != "null" ? local.json_data.parameters.cluster.admin.enable_etcd_ssd : "false"
  
  docker_username           = lookup(local.json_data.parameters.infrastructure,"docker","null") != "null" ? local.json_data.parameters.infrastructure.docker.DOCKER_USERNAME : ""
  docker_password           = lookup(local.json_data.parameters.infrastructure,"docker","null") != "null" ? local.json_data.parameters.infrastructure.docker.DOCKER_PASSWORD : ""

  consul_address            = lookup(local.json_data.parameters.infrastructure,"consul","null") != "null" ? local.json_data.parameters.infrastructure.consul.CONSUL_HTTP_ADDR : ""
  consul_token              = lookup(local.json_data.parameters.infrastructure,"consul","null") != "null" ? local.json_data.parameters.infrastructure.consul.CONSUL_HTTP_TOKEN : ""

  s3_service_endpoint       = lookup(local.json_data.parameters.infrastructure,"s3","null") != "null" ? local.json_data.parameters.infrastructure.s3.S3_SERVICE_ENDPOINT : ""
  s3_bucket_name            = lookup(local.json_data.parameters.infrastructure,"s3","null") != "null" ? local.json_data.parameters.infrastructure.s3.S3_BUCKET_NAME : ""
  s3_region                 = lookup(local.json_data.parameters.infrastructure,"s3","null") != "null" ? local.json_data.parameters.infrastructure.s3.S3_REGION : ""
  s3_access_key             = lookup(local.json_data.parameters.infrastructure,"s3","null") != "null" ? local.json_data.parameters.infrastructure.s3.S3_ACCESS_KEY : ""
  s3_secret_key             = lookup(local.json_data.parameters.infrastructure,"s3","null") != "null" ? local.json_data.parameters.infrastructure.s3.S3_SECRET_KEY : ""

  keycloack_cert            = lookup(local.json_data.parameters.infrastructure,"keycloack","null") != "null" ? local.json_data.parameters.infrastructure.keycloack.CERT : ""
  keycloack_key             = lookup(local.json_data.parameters.infrastructure,"keycloack","null") != "null" ? local.json_data.parameters.infrastructure.keycloack.KEY : ""
  keycloack_metadata        = lookup(local.json_data.parameters.infrastructure,"keycloack","null") != "null" ? local.json_data.parameters.infrastructure.keycloack.METADATA : ""

  vmc_endpoint_target1      = lookup(local.json_data.parameters.infrastructure,"vmc","null") != "null" ? local.json_data.parameters.infrastructure.vmc.VMC_ENDPOINT_TARGET1 : ""
  vmc_username              = lookup(local.json_data.parameters.infrastructure,"vmc","null") != "null" ? local.json_data.parameters.infrastructure.vmc.VMC_USERNAME : ""
  vmc_password              = lookup(local.json_data.parameters.infrastructure,"vmc","null") != "null" ? local.json_data.parameters.infrastructure.vmc.VMC_PASSWORD : ""
}
