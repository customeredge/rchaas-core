# Show Rancher private IPs
output "rancher_private_ips" {
     value = [module.rancher_openstack_rke.rancher_private_ips]
}

############################################################

# # Show cluster id
# output "cluster_id" {
#     value = module.rancher_openstack_rke.cluster_id
# }

# Show Kubeconfig file
output "rancher_kubeconfig" {
    value = module.rancher_openstack_rke.rancher_kubeconfig
}

# Show RKE cluster yaml
output "rke_cluster_yaml" {
    value = module.rancher_openstack_rke.rke_cluster_yaml
}

# Show RKE cluster yaml
output "rke_state" {
    value = module.rancher_openstack_rke.rke_state
}

############################################################

# Rancher Token Access key
output "rancher_access_key" {
 value = module.rancher_openstack_rke.rancher_access_key
}

# Rancher Token Secret key
output "rancher_secret_key" {
 value = module.rancher_openstack_rke.rancher_secret_key
}

# Rancher Token
output "rancher_token" {
 value = module.rancher_openstack_rke.rancher_token
}