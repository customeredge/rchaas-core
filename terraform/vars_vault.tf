###############################################################
###                   Common Providers
###############################################################

provider "vault" {
  version = "~> 2.15"
}

###############################################################
###                   Get Vault Secrets
###############################################################

data "vault_generic_secret" "openstack" {
  path = "${var.vault_kv_path}/openstack/${local.json_data.parameters.cluster.client.pf_zone}"
}

data "vault_generic_secret" "docker" {
  path = "${var.vault_kv_path}/docker"
}

data "vault_generic_secret" "consul" {
  path = "${var.vault_kv_path}/consul"
}

data "vault_generic_secret" "s3" {
  path = "${var.vault_kv_path}/s3"
}

data "vault_generic_secret" "rancher" {
  path = "${var.vault_kv_path}/rancher"
}

data "vault_generic_secret" "keycloack" {
  path = "${var.vault_kv_path}/keycloack"
}

data "vault_generic_secret" "vmc" {
  path = "${var.vault_kv_path}/vmc"
}

locals {
  script_version            = "0.2.0"

  json_data = jsondecode(file(var.parameters))

  os_domain_name            = data.vault_generic_secret.openstack.data["OS_DOMAIN_NAME"]
  os_auth_url               = data.vault_generic_secret.openstack.data["OS_AUTH_URL"]
  os_region_name            = data.vault_generic_secret.openstack.data["OS_REGION_NAME"]
  os_tenant_id              = data.vault_generic_secret.openstack.data["OS_TENANT_ID"]
  os_username               = data.vault_generic_secret.openstack.data["OS_USERNAME"]
  os_password               = data.vault_generic_secret.openstack.data["OS_PASSWORD"]
  os_private_key            = data.vault_generic_secret.openstack.data["OS_PRIVATE_KEY"]
  cluster_ca                = data.vault_generic_secret.openstack.data["CLUSTER_CA"]
  az_list                   = data.vault_generic_secret.openstack.data["az_list"]
  sg_default                = data.vault_generic_secret.openstack.data["sg_default"]
  sg_rancher                = data.vault_generic_secret.openstack.data["sg_rancher"]
  cloud                     = data.vault_generic_secret.openstack.data["cloud"]
  key_pair                  = data.vault_generic_secret.openstack.data["key_pair"]
  internal_network_name     = data.vault_generic_secret.openstack.data["internal_network_name"]
  external_network_name     = data.vault_generic_secret.openstack.data["external_network_name"]
  internal_subnet_id        = data.vault_generic_secret.openstack.data["internal_subnet_id"]
  external_subnet_id        = data.vault_generic_secret.openstack.data["external_subnet_id"]
  loadbalancer_rancher_eip  = data.vault_generic_secret.openstack.data["loadbalancer_rancher_eip"]
  
  enable_etcd_ssd           = lookup(local.json_data.parameters.cluster.admin,"enable_etcd_ssd","null") != "null" ? local.json_data.parameters.cluster.admin.enable_etcd_ssd : "false"

  docker_username           = data.vault_generic_secret.docker.data["DOCKER_USERNAME"]
  docker_password           = data.vault_generic_secret.docker.data["DOCKER_PASSWORD"]
  
  consul_address            = data.vault_generic_secret.consul.data["CONSUL_HTTP_ADDR"]
  consul_token              = data.vault_generic_secret.consul.data["CONSUL_HTTP_TOKEN"]

  s3_service_endpoint       = data.vault_generic_secret.s3.data["S3_SERVICE_ENDPOINT"]
  s3_bucket_name            = data.vault_generic_secret.s3.data["S3_BUCKET_NAME"]
  s3_region                 = data.vault_generic_secret.s3.data["S3_REGION"]
  s3_access_key             = data.vault_generic_secret.s3.data["S3_ACCESS_KEY"]
  s3_secret_key             = data.vault_generic_secret.s3.data["S3_SECRET_KEY"]

  rancher_cacert            = data.vault_generic_secret.rancher.data["CACERT"]
  rancher_cert              = data.vault_generic_secret.rancher.data["CERT"]
  rancher_key               = data.vault_generic_secret.rancher.data["KEY"]

  keycloack_cert            = data.vault_generic_secret.keycloack.data["CERT"]
  keycloack_key             = data.vault_generic_secret.keycloack.data["KEY"]
  keycloack_metadata        = data.vault_generic_secret.keycloack.data["METADATA"]

  vmc_endpoint_target1      = data.vault_generic_secret.vmc.data["VMC_ENDPOINT_TARGET1"]
  vmc_username              = data.vault_generic_secret.vmc.data["VMC_USERNAME"]
  vmc_password              = data.vault_generic_secret.vmc.data["VMC_PASSWORD"]
}

###############################################################
###                   Store Vault Secrets
###############################################################

resource "vault_generic_secret" "store_api_token" {
  path = "${trimsuffix(var.vault_kv_path,"/rchaas")}/k8saas/rancher"

  data_json = <<EOT
{
  "RANCHER_URL": "https://${local.json_data.parameters.cluster.client.rancher_url}/v3",
  "RANCHER_ACCESS_KEY": "${module.rancher_openstack_rke.rancher_access_key}",
  "RANCHER_SECRET_KEY": "${module.rancher_openstack_rke.rancher_secret_key}"
}
EOT
  
  depends_on = [
    module.rancher_openstack_rke.rancher_api_token,
    module.rancher_openstack_rke.etcd_snapshot_after
  ]
}