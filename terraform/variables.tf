# PARAMETERS
variable "parameters" {}

# CONSUL & S3
variable "backend_path" {}

# NOT USED ON MODULES
variable "vault_kv_path" {}