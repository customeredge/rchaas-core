#!/bin/bash
# v1.5.3

###################
#### Variables ####
###################

RED='\033[0;31m'
ORANGE='\033[0;33m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
END_COLOR='\033[0m'

INFO="[${BLUE}INFO${END_COLOR}]"
OK="[ ${GREEN}OK${END_COLOR} ]"
WARNING="[${ORANGE}WARN${END_COLOR}]"
ERROR="[${RED}ERROR${END_COLOR}]"


###################
#### Functions ####
###################

function add_proxy()
{
        # Add proxy
	echo -e "${INFO}  Adding proxy into .bashrc..."
	if [ ! -z "${http_proxy}" ] && [ ! -z "${https_proxy}" ]; then
        	if ! sudo grep -i "http_proxy" /home/${USER}/.bashrc 1>/dev/null 2>/dev/null; then cat <<EOF | tee -a /home/${USER}/.bashrc 2>&1 >/dev/null && echo -e "${OK}  Proxy for ${USER} user successfully added into ~/.bashrc." || { echo -e "${ERROR} Unable to add proxy for ${USER} user into ~/.bashrc." && exit 1; }

# Proxy
export HTTP_PROXY=${http_proxy}
export HTTPS_PROXY=${https_proxy}
export NO_PROXY=${no_proxy}

export http_proxy=${http_proxy}
export https_proxy=${https_proxy}
export no_proxy=${no_proxy}
EOF
        	else
			echo -e "${INFO}  Proxy already added for ${USER} user into ~/.bashrc."
		fi
                if ! sudo grep -i "http_proxy" /root/.bashrc 1>/dev/null 2>/dev/null; then cat <<EOF | sudo tee -a /root/.bashrc 2>&1 >/dev/null && echo -e "${OK}  Proxy for root user successfully added into ~/.bashrc." || { echo -e "${ERROR} Unable to add proxy for root user into ~/.bashrc." && exit 1; }

# Proxy
export HTTP_PROXY=${http_proxy}
export HTTPS_PROXY=${https_proxy}
export NO_PROXY=${no_proxy}

export http_proxy=${http_proxy}
export https_proxy=${https_proxy}
export no_proxy=${no_proxy}
EOF
        	else
			echo -e "${INFO}  Proxy already added for root user into ~/.bashrc."
		fi
	else
		echo -e "${INFO}  No proxy to add."
	fi
}

function disable_swap()
{
        echo -e "${INFO}  Disabling swap..."
        if SWAP_ENABLED=$(sudo grep swap /etc/fstab | grep -vE "^#"); then
                (sudo swapoff -a || sudo swapoff -a) && \
                sudo sed -rin '/\sswap\s/s/^#?/#/' /etc/fstab && \
                sudo mount -a && \
                echo -e "${OK}  Swap successfully disabled." || \
		{ echo -e "${ERROR} Unable to disable swap." && exit 1; }
        else
                echo -e "${INFO}  Swap already disabled."
        fi
}

function extend_var_lv()
{
	LV_SIZE="$1"

	echo -e "${INFO}  Extending /var LV..."
	VAR_SIZE=$(df /var|tail -1|awk {'print $4'})
	if [ ${VAR_SIZE} -le 8089000 ]; then
		#echo "You need at least 2GB of space on /var to store Docker images."
		#read -p "Extend /var LV (y/n)? [n] " CHOICE
       		#if [[ ${CHOICE} =~ ^[Y|y]$ ]]; then
			VAR=$(sudo df -h| grep '/var' | head -1)
			if [ ! -z "${VAR}" ]; then
				SIZE=$(echo "${VAR}" | awk '{print $2}' | tr -d 'G' | awk -F. '{print $1}')
				if [ ${SIZE} -lt 8 ]; then
					sudo lvextend -L ${LV_SIZE} /dev/rootvg/var_lv 2>/dev/null 1>/dev/null && \
       					sudo resize2fs /dev/rootvg/var_lv 2>/dev/null 1>/dev/null && \
					echo -e "${OK}  /var LV successfully extended." || \
					{ echo -e "${ERROR} Unable to extend /var LV." && exit 1; }
				else
					echo -e "${INFO}  /var LV already extended."	
				fi
			else
				echo -e "${INFO}  /var LV does not exists...ignoring."
			fi
		#else
		#	echo -e "${INFO}  Skipping /var LV extension."
		#fi
	else
                echo -e "${INFO}  /var LV does not need to extend."
        fi

}

function use_additional_storage()
{
        STORAGE_SIZE="$1"

        echo -e "${INFO}  Using additional storage..."
	FS=$(sudo fdisk -l 2>/dev/null|grep "/dev/[v|s]"|grep "${STORAGE_SIZE} GiB"|awk '{print $2}'|tr -d ":")
        if ! FS_TEST=$(sudo fdisk -l 2>/dev/null | grep "${FS}1"); then
                cat <<EOF |sudo fdisk ${FS}
n
p



p
w
EOF
                sleep 5 && \
		echo -e "${OK}  Additional storage (${STORAGE_SIZE} GiB) successfully used." || \
		{ echo -e "${ERROR} Unable to use additional storage (${STORAGE_SIZE} GiB)." && exit 1; }
	else
		echo -e "${INFO}  Additional storage (${STORAGE_SIZE} GiB) already in use."
	fi
}

function create_lv()
{
        LV_NAME="$1"
        LV_MOUNT_POINT="$2"
        LV_SIZE="$3"
        
	echo -e "${INFO}  Creating ${LV_NAME} LV..."
        if ! sudo grep "${LV_NAME}_lv" /etc/fstab 2>/dev/null 1>/dev/null; then
                FS=$(sudo fdisk -l 2>/dev/null|grep "/dev/[v|s]"|grep "${LV_SIZE} GiB"|awk '{print $2}'|tr -d ":") && \
                sudo pvcreate ${FS}1 && \
                sudo vgextend datavg ${FS}1 && \
                sudo lvcreate -L ${LV_SIZE}G -n ${LV_NAME}_lv datavg 2>/dev/null 1>/dev/null && \
                sudo mkfs.ext4 /dev/datavg/${LV_NAME}_lv 2>/dev/null 1>/dev/null && \
                echo "/dev/datavg/${LV_NAME}_lv      ${LV_MOUNT_POINT}           ext4 defaults,nodev 0 0" | sudo tee -a /etc/fstab 1>/dev/null 2>/dev/null && \
                sudo mkdir -p ${LV_MOUNT_POINT} && \
                sudo mount -a 2>/dev/null 1>/dev/null && \
                sudo chmod 755 ${LV_MOUNT_POINT} && \
                echo -e "${OK}  ${LV_NAME} LV successfully created." || \
                { echo -e "${ERROR} Unable to create ${LV_NAME} LV." && exit 1; }
        else
                echo -e "${INFO}  ${LV_NAME} LV already exists."
        fi
}

function create_fs()
{
        FS_NAME="$1"
        FS_MOUNT_POINT="$2"
        FS_SIZE="$3"

	echo -e "${INFO}  Mounting ${FS_NAME} FS..."
        FS=$(sudo fdisk -l 2>/dev/null|grep "/dev/[v|s]"|grep "${FS_SIZE} GiB"|awk '{print $2}'|tr -d ":")
        if ! sudo mount|grep "${FS_MOUNT_POINT}" 2>/dev/null 1>/dev/null; then
                sudo mkfs.ext4 ${FS}1 && \
                sudo mkdir -p ${FS_MOUNT_POINT} && \
                echo "${FS}1        ${FS_MOUNT_POINT}      ext4 defaults,nodev 0 0" | sudo tee -a /etc/fstab && \
                sudo mount -a && \
                echo -e "${OK}  ${FS_NAME} FS successfully mounted." || \
		{ echo -e "${ERROR} Unable to mount ${FS_NAME} FS." && exit 1; }
        else
                echo -e "${INFO}  ${FS_NAME} FS already mounted."
        fi
}

function install_docker()
{
        DOCKER_VERSION=$1
        REGISTRY_MIRROR=$2

        echo -e "${INFO}  Installing Docker Engine..."

        if ! D_VERSION=$(sudo docker version  2>/dev/null|grep "Version:" | head -1 | awk '{print $2}') || [ -z "${D_VERSION}" ]; then
                #if ! grep "download.docker.com" /etc/hosts 1>/dev/null 2>/dev/null; then echo "143.204.226.13 download.docker.com"; fi | sudo tee -a /etc/hosts 2>/dev/null 1>/dev/null && \
                export http_proxy=${http_proxy} && export https_proxy=${https_proxy} && export no_proxy=${no_proxy} && \
                export VERSION=${DOCKER_VERSION} && curl -fsSL https://get.docker.com -o get-docker.sh && sudo touch /etc/apt/sources.list.d/docker.list && sudo chmod 644 /etc/apt/sources.list.d/docker.list && sudo touch /usr/share/keyrings/docker-archive-keyring.gpg && sudo chmod 644 /usr/share/keyrings/docker-archive-keyring.gpg && sudo -E sh get-docker.sh && rm -f get-docker.sh && \
                #curl -O https://releases.rancher.com/install-docker/${DOCKER_VERSION}.sh && chmod +x ${DOCKER_VERSION}.sh && sudo DEBIAN_FRONTEND=noninteractive ./${DOCKER_VERSION}.sh && rm -f ./${DOCKER_VERSION}.sh
                sudo mkdir -p /etc/systemd/system/docker.service.d 1>/dev/null 2>/dev/null && \
                if [ ! -z "${http_proxy}" ] && [ ! -z "${https_proxy}" ] && [ ! -z "${no_proxy}" ];then cat <<EOF | sudo tee /etc/systemd/system/docker.service.d/http-proxy.conf 1>/dev/null 2>/dev/null; fi
[Service]
Environment="HTTP_PROXY=${http_proxy}" "HTTPS_PROXY=${https_proxy}" "NO_PROXY=${no_proxy}"
EOF
                if [ ! -z "${REGISTRY_MIRROR}" ]; then REGISTRY_MIRROR_LINE="\"registry-mirrors\": [\"${REGISTRY_MIRROR}\"]" && COMMA=","; else REGISTRY_MIRROR_LINE="" && COMMA=""; fi
                cat <<EOF | sudo tee /etc/docker/daemon.json 1>/dev/null 2>/dev/null
{
	"log-driver": "json-file",
	"log-opts": {
		"max-size": "10m",
		"max-file": "3"
	}${COMMA}
        ${REGISTRY_MIRROR_LINE}
} 
EOF
                sudo systemctl daemon-reload 1>/dev/null 2>/dev/null && \
                sudo systemctl restart docker 1>/dev/null 2>/dev/null && \
                VERSION=$(sudo docker version  2>/dev/null|grep "Version:" | head -1 | awk '{print $2}') && \
                echo -e "${OK}  Docker Engine \"${VERSION}\" successfully installed." || \
                { echo -e "${ERROR} Unable to install Docker Engine." && exit 1; }
        else
                VERSION=${D_VERSION}
                echo -e "${INFO}  Docker Engine \"${VERSION}\" already installed."
        fi
}

function add_user_docker_group()
{
        echo -e "${INFO}  Adding user \"${USER}\" to Docker group..."
        if ! grep docker /etc/group| grep ${USER} 2>/dev/null 1>/dev/null; then
                # Allow simple user to execute docker commands
                sudo usermod -aG docker $USER 1>/dev/null 2>/dev/null && \
                echo -e "${OK}  User \"${USER}\" successfully added to Docker group." || \
                { echo -e "${ERROR} Unable to add \"${USER}\" to Docker group." && exit 1; }
        else
                echo -e "${INFO}  User \"${USER}\" already added to Docker group."
        fi
}

function enable_gpu()
{
        echo -e "${INFO}  Enabling GPU support..."
        if ! NVDOCKER=$(dpkg -l|grep nvidia-docker2); then
                sudo -E add-apt-repository -y ppa:graphics-drivers/ppa && \
                sudo apt update && \
                sudo apt install -y ubuntu-drivers-common && \
                sudo ubuntu-drivers autoinstall && \
                sudo cat /etc/docker/daemon.json | sed -e 's|}$|},\n\t"default-runtime": "nvidia",\n\t"runtimes": {\n\t\t"nvidia": {\n\t\t\t"path": "/usr/bin/nvidia-container-runtime",\n\t\t\t"runtimeArgs": []\n\t\t}\n\t}|g' | sudo tee /etc/docker/daemon.json && \
                distribution=$(. /etc/os-release;echo $ID$VERSION_ID) && \
                curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - && \
                curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list && \
                sudo apt-get update && sudo apt-get install -y nvidia-docker2 && \
                sudo systemctl daemon-reload && \
                sudo systemctl restart docker && \
                echo -e "${OK}  GPU support successfully enabled." || \
                { echo -e "${ERROR} Unable to enable GPU support." && exit 1; }
        else
                echo -e "${INFO}  GPU support already enabled."
        fi
}

function rke_hardening_all_nodes()
{
        
        echo -e "${INFO}  Applying \"RKE Hardening all nodes\"..."
        if [ ! -f "/etc/sysctl.d/90-kubelet.conf" ]; then
                cat <<EOF | sudo tee /etc/sysctl.d/90-kubelet.conf 1>/dev/null 2>/dev/null && sudo sysctl -p /etc/sysctl.d/90-kubelet.conf && echo -e "${OK}  \"RKE Hardening all nodes\" successfully applied." || { echo -e "${ERROR} Unable to apply \"RKE Hardening all nodes\"." && exit 1; }
vm.overcommit_memory=1
vm.panic_on_oom=0
kernel.panic=10
kernel.panic_on_oops=1
kernel.keys.root_maxbytes=25000000
EOF
        else
                echo -e "${INFO}  \"RKE Hardening all nodes\" already applied."
        fi
}

function rke_hardening_etcd_nodes()
{
        echo -e "${INFO}  Applying \"RKE Hardening etcd nodes\"..."
        if ! ETCD_USER=$(sudo cat /etc/passwd|grep etcd); then
                sudo groupadd --gid 52034 etcd && \
                sudo useradd --comment "etcd service account" --uid 52034 --gid 52034 etcd && \
                echo -e "${OK}  \"RKE Hardening etcd nodes\" successfully applied." || \
                { echo -e "${ERROR} Unable to apply \"RKE Hardening etcd nodes\"." && exit 1; }
        else
                echo -e "${INFO}  \"RKE Hardening etcd nodes\" already applied."
        fi
}

function install_rancher_rke()
{
        RANCHER_HOSTNAME="$1"
        RANCHER_NODES="$2"
        RANCHER_RKE_CONFIG="$3"

        echo -e "${INFO}  Deploying Rancher RKE..."
        if ! kubectl get nodes 2>/dev/null 1>/dev/null; then
                curl -JLO https://github.com/rancher/rke/releases/download/v1.0.4/rke_linux-amd64 && \
                chmod +x rke_linux-amd64 && \
                sudo mv rke_linux-amd64 /usr/bin/rke && \
                #ssh-keygen -f /home/${USER}/.ssh/id_rsa -N "" && \
                #ssh-copy-id -i /home/${USER}/.ssh/id_rsa ${RANCHER_NODES} && \
                rke up --config=${RANCHER_RKE_CONFIG} && \
                #rke up --config=${RANCHER_RKE_CONFIG} --custom-certs --cert-dir /tmp/certs && \
                curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && \
                chmod +x ./kubectl && \
                sudo mv ./kubectl /usr/bin/ && \
                curl -JLO https://get.helm.sh/helm-v3.1.0-linux-amd64.tar.gz && \
                tar xvzf helm-v3.1.0-linux-amd64.tar.gz && \
                rm helm-v3.1.0-linux-amd64.tar.gz && \
                sudo mv linux-amd64/helm /usr/bin/ && \
                rm -rf linux-amd64 && \
                helm repo add rancher-stable https://releases.rancher.com/server-charts/stable && \
                mkdir -p /home/${USER}/.kube && mv /tmp/kube_config_cluster.yml /home/${USER}/.kube/config && \
                kubectl create namespace cattle-system && \
                kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.12/deploy/manifests/00-crds.yaml && \
                kubectl create namespace cert-manager && \
                helm repo add jetstack https://charts.jetstack.io && \
                helm repo update && \
                helm install cert-manager jetstack/cert-manager --wait \
                --namespace cert-manager \
                --version v0.12.0 && \
                helm install rancher rancher-stable/rancher --wait \
                --namespace cattle-system \
                --set hostname=${RANCHER_HOSTNAME} \
                --set proxy=${http_proxy} && \
                #kubectl -n cattle-system exec $(kubectl -n cattle-system get pods -l app=rancher | grep '1/1' | head -1 | awk '{ print $1 }') -- reset-password 2>/dev/null | tail -1 && \
                echo -e "${OK}  Rancher RKE successfully deployed." || \
                { echo -e "${ERROR} Unable to deploy Rancher RKE." && exit 1; }
        else
                echo -e "${INFO}  Rancher RKE already deployed."
        fi
}

function configure_rancher()
{
	RANCHER_SERVER_IP=$1
	
        echo -e "${INFO}  Configuring Rancher..."
        if ! CONFIGURED=$(ls /tmp | grep APITOKEN) || [ -z "${CONFIGURED}" ]; then
                RANCHER_IP="127.0.0.1" && \
                #RANCHER_LOCAL_IP=$(hostname -I | awk '{print $1}') && \
		RANCHER_EXTERNAL_PORT="8443" && \
                RANCHER_PORT="443" && \
                RANCHER_SERVER="https://${RANCHER_SERVER_IP}:${RANCHER_EXTERNAL_PORT}" && \
                RANCHER_PASSWORD="admin" && \
                curlimage="appropriate/curl" && \
                jqimage="stedolan/jq" && \
                DOCKER_PULL=$(sudo -E docker pull $curlimage && sudo -E docker pull $jqimage) && \
                LOGINRESPONSE=$(eval "sudo -E docker exec rancher_server curl -s \"https://${RANCHER_IP}:${RANCHER_PORT}/v3-public/localProviders/local?action=login\" -H 'content-type: application/json' --data-binary '{\"username\":\"admin\",\"password\":\"${RANCHER_PASSWORD}\"}' --insecure") && sleep 2 && \
                LOGINTOKEN=$(echo $LOGINRESPONSE | sudo -E docker run --rm -i $jqimage -r .token) && \
                eval "sudo -E docker exec rancher_server curl -s \"https://${RANCHER_IP}:${RANCHER_PORT}/v3/users?action=changepassword\" -H 'content-type: application/json' -H \"Authorization: Bearer $LOGINTOKEN\" --data-binary '{\"currentPassword\":\"admin\",\"newPassword\":\"'\"${RANCHER_PASSWORD}\"'\"}' --insecure" 2>/dev/null 1>/dev/null && sleep 2 && \
                APIRESPONSE=$(eval "sudo -E docker exec rancher_server curl -s 'https://${RANCHER_IP}:${RANCHER_PORT}/v3/token' -H 'content-type: application/json' -H 'Authorization: Bearer $LOGINTOKEN' --data-binary '{\"type\":\"token\",\"description\":\"automation\"}' --insecure") && sleep 2 && \
                APITOKEN=$(echo $APIRESPONSE | sudo -E docker run --rm -i $jqimage -r .token) && \
                eval "sudo -E docker exec rancher_server curl -s 'https://${RANCHER_IP}:${RANCHER_PORT}/v3/settings/server-url' -H 'content-type: application/json' -H 'Authorization: Bearer $APITOKEN' -X PUT --data-binary '{\"name\":\"server-url\",\"value\":\"'\"${RANCHER_SERVER}\"'\"}' --insecure" 2>/dev/null 1>/dev/null && sleep 2 && \
		echo "${RANCHER_SERVER_IP},${RANCHER_EXTERNAL_PORT},${APITOKEN}" > /tmp/RANCHER && \
                #VERSION=$(sudo docker version  2>/dev/null|grep "Version:" | head -1 | awk '{print $2}') && \
                echo -e "${OK}  Rancher successfully configured." || \
                { echo -e "${ERROR} Unable to configure Rancher." && exit 1; }
        else
                #VERSION=$(echo ${K_VERSION}|grep "Version:" | head -1 | awk '{print $3}')
                echo -e "${INFO}  Rancher already configured."
        fi
}


function install_k3s()
{
	K3S_SERVER=$1
	K3S_TOKEN=$2

        echo -e "${INFO}  Installing K3S..."
        if ! K_VERSION=$(k3s --version  2>/dev/null | awk '{print $3}') || [ -z "${K_VERSION}" ]; then
                (export http_proxy=${http_proxy} && export https_proxy=${https_proxy} && export no_proxy=${no_proxy} && \
                ([ -z "${K3S_SERVER}" ] && curl -sfL https://get.k3s.io | sh - 1>/dev/null 2>/dev/null && sleep 5) || \
		#(curl -sOL https://github.com/rancher/k3s/releases/download/v1.17.0%2Bk3s.1/k3s 1>/dev/null 2>/dev/null && sudo mv k3s /usr/local/bin && chmod +x /usr/local/bin/k3s && \
		curl -sfL https://get.k3s.io | K3S_URL=https://${K3S_SERVER}:6443 K3S_TOKEN=${K3S_TOKEN} sh - && \
                echo "sudo /usr/local/bin/k3s agent --server https://${K3S_SERVER}:6443 --token ${K3S_TOKEN}" && \
		sudo /usr/local/bin/k3s agent --server https://${K3S_SERVER}:6443 --token ${K3S_TOKEN} &) && \
                sleep 5 && \
                VERSION=$(k3s --version  2>/dev/null | awk '{print $3}') && \
                echo -e "${OK}  K3S \"${VERSION}\" successfully installed." || \
                { echo -e "${ERROR} Unable to install K3S." && exit 1; }
        else
                echo -e "${INFO}  K3S \"${K_VERSION}\" already installed."
        fi
}

function add_k3s_rancher()
{
	RANCHER_SERVER_IP=$1
	K3S_NAME=$2

	PRIVATE_KEY="Training"

	echo -e "${INFO}  Adding K3S to Rancher..."
        if RANCHER_ACCESS=$(ssh -o StrictHostKeyChecking=no -i /tmp/$PRIVATE_KEY ${RANCHER_SERVER_USER}@${RANCHER_SERVER_IP} "cat /tmp/RANCHER"); then
		echo "${RANCHER_ACCESS}" && \
		RANCHER_IP=$(echo "${RANCHER_ACCESS}" | awk -F, '{print $1}') && \
		RANCHER_PORT=$(echo "${RANCHER_ACCESS}" | awk -F, '{print $2}') && \
		APITOKEN=$(echo "${RANCHER_ACCESS}" | awk -F, '{print $3}') && \
		curlimage="appropriate/curl" && \
		jqimage="stedolan/jq" && \
		DOCKER_PULL=$(sudo -E docker pull $curlimage && sudo -E docker pull $jqimage) && \
		CLUSTERRESPONSE=$(eval "sudo -E docker run --rm -i $curlimage -s -k 'https://${RANCHER_IP}:${RANCHER_PORT}/v3/clusters' -H 'Content-Type: application/json' -H 'Authorization: Bearer $APITOKEN' --data-binary '{\"amazonElasticContainerServiceConfig\":null, \"azureKubernetesServiceConfig\":null, \"dockerRootDir\":\"/var/lib/docker\", \"enableClusterAlerting\":false, \"enableClusterMonitoring\":false, \"googleKubernetesEngineConfig\":null, \"localClusterAuthEndpoint\":null, \"name\":\"${K3S_NAME}\", \"rancherKubernetesEngineConfig\":null, \"internal\": false}'") && sleep 2 && \
		CLUSTERID=$(echo $CLUSTERRESPONSE | sudo -E docker run --rm -i $jqimage -r .id) && \
		eval "sudo -E docker run --rm -i $curlimage -s 'https://${RANCHER_IP}:${RANCHER_PORT}/v3/clusterregistrationtoken' -H 'content-type: application/json' -H 'Authorization: Bearer $APITOKEN' --data-binary '{\"type\":\"clusterRegistrationToken\",\"clusterId\":\"'$CLUSTERID'\"}' --insecure" 2>/dev/null 1>/dev/null && sleep 2 && \
		INSECURECOMMAND=$(eval "sudo -E docker run --rm -i $curlimage -s -k 'https://${RANCHER_IP}:${RANCHER_PORT}/v3/clusterregistrationtoken' -H 'content-type: application/json' -H 'Authorization: Bearer $APITOKEN' | sudo -E docker run --rm -i $jqimage -r '.data[].insecureCommand' | tail -1") && sleep 2 && \
		COMMAND=$(echo ${INSECURECOMMAND}|sed "s|kubectl|sudo /usr/local/bin/kubectl|g") && \
		eval "${COMMAND}" 2>/dev/null 1>/dev/null && \
                #VERSION=$(sudo docker version  2>/dev/null|grep "Version:" | head -1 | awk '{print $2}') && \
                echo -e "${OK}  K3S successfully added to Rancher." || \
                { echo -e "${ERROR} Unable to add K3S to Rancher." && exit 1; }
        else
                #VERSION=$(echo ${K_VERSION}|grep "Version:" | head -1 | awk '{print $3}')
                echo -e "${INFO}  K3S already added to Rancher."
        fi

}

function remove_rancher_node()
{
        RANCHER_API_ENDPOINT=$1
        RANCHER_API_CREDENTIALS=$2
        NODE_NAME=$3

        echo -e "${INFO}  Removing Rancher node ${NODE_NAME}..."

        if [ -z "${RANCHER_API_ENDPOINT}" ] || [ -z "${RANCHER_API_CREDENTIALS}" ] || [ -z "${NODE_NAME}" ]; then
                echo -e "${ERROR} Arguments missing (RANCHER_API_ENDPOINT, RANCHER_API_CREDENTIALS or NODE_NAME)." && exit 1;
        fi

        NODE_ID=$(eval "curl -k -s -u \"${RANCHER_API_CREDENTIALS}\" ${RANCHER_API_ENDPOINT}/nodes | jq -r '.data | map(select(.requestedHostname == \"${NODE_NAME}\")) | .[].id'")

        if ! curl -k -s -u "${RANCHER_API_CREDENTIALS}" ${RANCHER_API_ENDPOINT}/nodes/${NODE_ID} | jq '.message' | grep "not found" 2>/dev/null 1>/dev/null; then
                echo "curl -s -X DELETE -k -u \"RANCHER_API_CREDENTIALS\" ${RANCHER_API_ENDPOINT}/nodes/${NODE_ID} | jq '.'" && \
                CURL_CMD=$(curl -s -X DELETE -k -u "${RANCHER_API_CREDENTIALS}" ${RANCHER_API_ENDPOINT}/nodes/${NODE_ID} | jq '.' | sed 's|$|\\n|') && \
                echo -e ${CURL_CMD} | grep "\"baseType\": \"node\"" 2>/dev/null 1>/dev/null && \
                echo -e "${OK}  Rancher node ${NODE_NAME} successfully removed." || \
                { echo -e ${CURL_CMD} && echo -e "${ERROR} Unable to remove Rancher node ${NODE_NAME}." && exit 1; }
        else
                echo -e "${INFO}  Rancher node ${NODE_NAME} does not exists."
        fi
}

function show_title()
{
        TITLE="$1"

        TITLE_SIZE=$(echo "${TITLE}"| wc -c)
        NB_SPACES=$(( ((70-${TITLE_SIZE})/2) ))
        NB_SPACES=$(( ${NB_SPACES}+2 ))
        if ((${TITLE_SIZE}%2)); then
                NB_SPACES_RIGHT=$(( ${NB_SPACES}+1 ))
        else
                NB_SPACES_RIGHT=${NB_SPACES}
        fi

        #TOTAL="$(( ${NB_SPACES}+${TITLE_SIZE}+${NB_SPACES_RIGHT} ))"
        #echo "${NB_SPACES} + ${TITLE_SIZE} + ${NB_SPACES_RIGHT} = ${TOTAL}"

        echo ""
        echo "==========================================================================="
        echo -n "#"
        for i in `seq -w 1 ${NB_SPACES}`; do
                echo -n " "
        done
        echo -e -n "${PURPLE}${TITLE}${END_COLOR}"
        for i in `seq -w 1 ${NB_SPACES_RIGHT}`; do
                echo -n " "
        done
        echo "#"
        echo "==========================================================================="
}

function show_usage()
{
        echo ""
        echo "Usage: $0 [-h -v [-R or -K] -I RANCHER_IP -r NODE_NAME ...]"
        echo ""
        echo "A deployment kit for Rancher and K3S"
        echo ""
        echo "Options:"
        echo -e "    -a, --hardening-all\t\t\tApply RKE Hardening all nodes"
        echo -e "    -e, --hardening-etcd\t\t\tApply RKE Hardening etcd nodes"
        echo -e "    -g, --gpu\t\t\t\tEnable GPU support"
        echo -e "    -m, --registry-mirror\t\t\tUse a registry mirror"
        echo -e "    -t, --use-additional-storage\t\tUse additional storage"
        echo -e "    -o, --docker-version\t\t\tDocker version"
        echo -e "    -P, --prerequisites\t\t\tDeploy prerequisites"
        echo -e "    -r, --remove-node NODE_NAME\t\tRemove Rancher Node"
        echo -e "    -C, --rancher-rke-config RANCHER_RKE_CONFIG\t\tRancher RKE config file"
        echo -e "    -G, --rancher-nodes RANCHER_NODES\t\tRancher Nodes"
        echo -e "    -H, --rancher-hostname RANCHER_HOSTNAME\t\tRancher Hostname"
        echo -e "    -I, --rancher-ip RANCHER_IP\t\tRancher IP"
        echo -e "    -K, --k3s\t\t\t\tDeploy K3S"
        echo -e "    -U, --user\t\t\t\tUser"
        echo -e "    -S, --ssh-rancher-user\t\tSSH Rancher User"
	echo -e "    -h, --help\t\t\t\tPrint usage"
        echo -e "    -v, --version\t\t\tPrint version information and quit"
        echo ""
        echo "Examples:"
        echo -e "    # Deployment of prerequisites and Rancher"
        echo -e "    $0 -P -R -I 90.84.46.184"
        echo ""
        echo -e "    # Deployment of prerequisites and K3S"
        echo -e "    $0 -P -K -I 90.84.46.184"
        echo ""
        echo -e "    # Show this help"
        echo -e "    $0 -h"
        echo ""
}


###################
####    Main   ####
###################

RANCHER_SERVER_IP="127.0.0.1"
DOCKER_VERSION="19.03"
RANCHER_SERVER_USER="cloud"
K3S_NAME="k3s"
K3S_SERVER=""
K3S_TOKEN=""
REGISTRY_MIRROR=""
SLEEP="120"
PREREQ=0
RANCHER=0
K3S=0
REMOVE_RANCHER_NODE=0
GPU=0
HARDENING_RKE=0
HARDENING_ALL=0
HARDENING_ETCD=0
CREATE_LV=0
ADD_STORAGE=0
FS_COUNT=0
STORAGE_COUNT=0
CREATE_FS=0

DISTRIB=$(cat /etc/*-release|grep "^ID="|awk -F= '{print $2}'|tr -d "\"")

if LVM=$(sudo lvdisplay|wc -l) && [ ${LVM} -ne 0 ]; then
        CREATE_LV=1
fi

while :; do
        case $1 in
                -a|--hardening-all)
                        HARDENING_RKE=1
                        HARDENING_ALL=1
                        ;;
                # use-additional-storag
                -t|--use-additional-storage)
                        ADD_STORAGE=1
                        if [ "$2" ]; then
                                STORAGE_SIZE[STORAGE_COUNT]=$2
                                STORAGE_COUNT=$(( ${STORAGE_COUNT} + 1 ))
                                shift
                        else
                                echo -e "${ERROR} --use-additional-storage requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --use-additional-storage=?*)
                        ADD_STORAGE=1
                        STORAGE_SIZE[STORAGE_COUNT]=${1#*=}
                        STORAGE_COUNT=$(( ${STORAGE_COUNT} + 1 ))
                        ;;
                --use-additional-storage=)
                        echo -e "${ERROR} --use-additional-storage requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # create-fs
                -f|--create-fs)
                        CREATE_FS=1
                        if [ "$2" ]; then
                                FS_NAME[FS_COUNT]=$(echo $1 | cut -d',' -f 1)
                                FS_MOUNT_POINT[FS_COUNT]=$(echo $1 | cut -d',' -f 2)
                                FS_SIZE[FS_COUNT]=$(echo $1 | cut -d',' -f 3)
                                FS_COUNT=$(( ${FS_COUNT} + 1 ))
                                shift
                        else
                                echo -e "${ERROR} --create-fs requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --create-fs=?*)
                        CREATE_FS=1
                        FS_NAME[FS_COUNT]=$(echo ${1#*=} | cut -d',' -f 1)
                        FS_MOUNT_POINT[FS_COUNT]=$(echo ${1#*=} | cut -d',' -f 2)
                        FS_SIZE[FS_COUNT]=$(echo ${1#*=} | cut -d',' -f 3)
                        FS_COUNT=$(( ${FS_COUNT} + 1 ))
                        ;;
                --create-fs=)
                        echo -e "${ERROR} --create-fs requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                #############
                -e|--hardening-etcd)
                        HARDENING_RKE=1
                        HARDENING_ETCD=1
                        ;;
                -g|--gpu)
                        GPU=1
                        ;;
                -h|--help)
                        show_usage
                        exit 0
                        ;;
		-P|--prerequisites)
                        PREREQ=1
                        ;;
                -R|--rancher)
                        RANCHER=1
			;;
		-K|--k3s)
                        K3S=1
                        ;;
                # registry-mirror
                -m|--registry-mirror)
                        if [ "$2" ]; then
                                REGISTRY_MIRROR=$2
                                shift
                        else
                                echo -e "${ERROR} --registry-mirror requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --registry-mirror=?*)
                        REGISTRY_MIRROR=${1#*=}
                        ;;
                --registry-mirror=)
                        echo -e "${ERROR} --registry-mirror requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # remove nodes
                -r|--remove-node)
                        REMOVE_RANCHER_NODE=1
                        if [ "$2" ]; then
                                NODE_NAME=$2
                                shift
                        else
                                echo -e "${ERROR} --remove-node requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                 --remove-node=?*)
                        REMOVE_RANCHER_NODE=1
                        NODE_NAME=${1#*=}
                        ;;
                --remove-node=)
                        echo -e "${ERROR} --remove-node requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # docker version
                -o|--docker-version)
                        if [ "$2" ]; then
                                DOCKER_VERSION=$2
                                shift
                        else
                                echo -e "${ERROR} --docker-version requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                 --docker-version=?*)
                        DOCKER_VERSION=${1#*=}
                        ;;
                --docker-version=)
                        echo -e "${ERROR} --docker-version requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # rancher rke config
                -C|--rancher-rke-config)
                        if [ "$2" ]; then
                                RANCHER_RKE_CONFIG=$2
                                shift
                        else
                                echo -e "${ERROR} --rancher-rke-config requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --rancher-rke-config=?*)
                        RANCHER_RKE_CONFIG=${1#*=}
                        ;;
                --rancher-nodes=)
                        echo -e "${ERROR} --rancher-rke-config requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # rancher node
                -G|--rancher-nodes)
                        if [ "$2" ]; then
                                RANCHER_NODES=$2
                                shift
                        else
                                echo -e "${ERROR} --rancher-nodes requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --rancher-nodes=?*)
                        RANCHER_NODES=${1#*=}
                        ;;
                --rancher-nodes=)
                        echo -e "${ERROR} --rancher-nodes requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # rancher hostname
                -H|--rancher-hostname)
                        if [ "$2" ]; then
                                RANCHER_HOSTNAME=$2
                                shift
                        else
                                echo -e "${ERROR} --rancher-hostname requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --rancher-hostname=?*)
                        RANCHER_HOSTNAME=${1#*=}
                        ;;
                --rancher-hostname=)
                        echo -e "${ERROR} --rancher-hostname requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # rancher IP
                -I|--rancher-ip)
                        if [ "$2" ]; then
                                RANCHER_SERVER_IP=$2
                                shift
                        else
                                echo -e "${ERROR} --rancher-ip requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --rancher-ip=?*)
                        RANCHER_SERVER_IP=${1#*=}
                        ;;
                --rancher-ip=)
                        echo -e "${ERROR} --rancher-ip requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # user
                -U|--user)
                        if [ "$2" ]; then
                                USER=$2
                                shift
                        else
                                echo -e "${ERROR} --user requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --user=?*)
                        USER=${1#*=}
                        ;;
                --user=)
                        echo -e "${ERROR} --user requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # ssh rancher user
                -S|--ssh-rancher-user)
                        if [ "$2" ]; then
                                RANCHER_SERVER_USER=$2
                                shift
                        else
                                echo -e "${ERROR} --ssh-rancher-user requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --ssh-rancher-user=?*)
                        RANCHER_SERVER_USER=${1#*=}
                        ;;
                --ssh-rancher-user=)
                        echo -e "${ERROR} --ssh-rancher-user requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # k8s name
                -N|--k3s-name)
                        if [ "$2" ]; then
                                K3S_NAME=$2
                                shift
                        else
                                echo -e "${ERROR} --k3s-name requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --k3s-name=?*)
                        K3S_NAME=${1#*=}
                        ;;
                --k3s-name=)
                        echo -e "${ERROR} --k3s-name requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # sleep
                -s|--sleep)
                        if [ "$2" ]; then
                                SLEEP=$2
                                shift
                        else
                                echo -e "${ERROR} --sleep requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --sleep=?*)
                        SLEEP=${1#*=}
                        ;;
                --sleep=)
                        echo -e "${ERROR} --sleep requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # k3s token
                -T|--k3s-token)
                        if [ "$2" ]; then
                                K3S_TOKEN=$2
                                shift
                        else
                                echo -e "${ERROR} --k3s-token requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --k3s-token=?*)
                        K3S_TOKEN=${1#*=}
                        ;;
                --k3s-token=)
                        echo -e "${ERROR} --k3s-token requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                # k3s server
		-V|--k3s-server)
                        if [ "$2" ]; then
                                K3S_SERVER=$2
                                shift
                        else
                                echo -e "${ERROR} --k3s-server requires a non-empty argument."
                                show_usage
                                exit 1
                        fi
                        ;;
                --k3s-server=?*)
                        K3S_SERVER=${1#*=}
                        ;;
                --k3s-server=)
                        echo -e "${ERROR} --k3s-server requires a non-empty argument."
                        show_usage
                        exit 1
                        ;;
                ##############
                *)
                        break
        esac

        shift
done


if [ "${PREREQ}" -eq 1 ]; then
	# Add proxy
	show_title "ADD PROXY"
	add_proxy

        # Disable swap
        show_title "DISABLE SWAP"
	disable_swap

        if [ "${ADD_STORAGE}" -eq 1 ]; then
                for STORAGE_SIZE in ${STORAGE_SIZE[@]}; do
                        # Use additional storage
                        show_title "USE ADDITIONAL STORAGE"
                        use_additional_storage "${STORAGE_SIZE}"
                done
        fi

        if [ "${CREATE_FS}" -eq 1 ]; then
                COUNT=0
                for FS_NAME in ${FS_NAME[@]}; do
                        if [ "${CREATE_LV}" -eq 1 ]; then
                                # Create LV
                                show_title "CREATE ${FS_NAME[${COUNT}]} LV"
                                create_lv "${FS_NAME[${COUNT}]}" "${FS_MOUNT_POINT[${COUNT}]}" "${FS_SIZE[${COUNT}]}"
                        else
                                # Create FS
                                show_title "CREATE ${FS_NAME[${COUNT}]} FS"
                                create_fs "${FS_NAME[${COUNT}]}" "${FS_MOUNT_POINT[${COUNT}]}" "${FS_SIZE[${COUNT}]}"
                        fi
                        COUNT=$(( ${COUNT} + 1 ))
                done
        fi

	# Install Docker Engine
	show_title "INSTALL DOCKER"
	install_docker "${DOCKER_VERSION}" "${REGISTRY_MIRROR}"

	# Add standard user to Docker group
	show_title "ADD USER DOCKER GROUP"
	add_user_docker_group
fi

if [ "${HARDENING_RKE}" -eq 1 ]; then
	# Apply RKE Hardening all nodes
	show_title "RKE HARDENING ALL NODES"
	rke_hardening_all_nodes

        # Apply RKE Hardening etcd nodes
	show_title "RKE HARDENING ETCD NODES"
	rke_hardening_etcd_nodes
fi

if [ "${GPU}" -eq 1 ]; then
	# Enable GPU
	show_title "ENABLE GPU SUPPORT"
	enable_gpu
fi

if [ "${RANCHER}" -eq 1 ]; then
	show_title "INSTALL RANCHER RKE"
	install_rancher_rke "${RANCHER_HOSTNAME}" "${RANCHER_NODES}" "${RANCHER_RKE_CONFIG}"

	# echo -e "\nSleeping ${SLEEP}s..."
	# sleep ${SLEEP}
	# echo "Sleep finished!"

	# # Install configure Rancher Server
	# show_title "CONFIGURE RANCHER SERVER"
	# configure_rancher "${RANCHER_SERVER_IP}"
fi

if [ "${K3S}" -eq 1 ]; then
	# Install K3S
	show_title "INSTALL K3S"
	install_k3s "${K3S_SERVER}" "${K3S_TOKEN}"

	echo -e "\nSleeping ${SLEEP}s..."
	sleep ${SLEEP}
	echo "Sleep finished!"

	# Add K3S to Rancher
	#show_title "ADD K3S TO RANCHER"
	#add_k3s_rancher "${RANCHER_SERVER_IP}" "${K3S_NAME}"
fi

if [ "${REMOVE_RANCHER_NODE}" -eq 1 ]; then
        # Remove Rancher Node
        show_title "REMOVE RANCHER NODE"
        remove_rancher_node "${RANCHER_API_ENDPOINT}" "${RANCHER_API_CREDENTIALS}" "${NODE_NAME}"
fi
