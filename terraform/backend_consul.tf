terraform {
  backend "consul" {
    scheme  = "https"
    path    = "SET_IN_GITLAB_CI"
  }
}
