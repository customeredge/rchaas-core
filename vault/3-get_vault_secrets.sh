#################################################################################################################
export CLOUDS="fe vdr"
export ENVS="di:qual pr:prod"
export ZONES="exec admin cs"

for CLOUD in ${CLOUDS}; do
    for ENV_FULL in ${ENVS}; do
        ENV=${ENV_FULL%:*}
        PF_ENV=${ENV_FULL#*:}

        source ./1-get_vault_token.sh
                
        echo ""
        echo "CLOUD: ${CLOUD}"
        echo "ENV: ${ENV}"
        echo "vault_kv_path: ${vault_kv_path}"

        for ZONE in ${ZONES};do
            # OPENSTACK
            vault kv get -tls-skip-verify ${vault_kv_path}/openstack/${ZONE}
        done
        
        # GOOGLE
        vault kv get -tls-skip-verify ${vault_kv_path}/google

        # DOCKER
        vault kv get -tls-skip-verify ${vault_kv_path}/docker

        # CONSUL
        vault kv get -tls-skip-verify ${vault_kv_path}/consul

        # S3
        vault kv get -tls-skip-verify ${vault_kv_path}/s3

        # POWERDNS
        vault kv get -tls-skip-verify ${vault_kv_path}/powerdns

        # RANCHER
        vault kv get -tls-skip-verify ${vault_kv_path}/rancher

        # KEYCLOAK
        vault kv get -tls-skip-verify ${vault_kv_path}/keycloak

        # VMC
        vault kv get -tls-skip-verify ${vault_kv_path}/vmc
    done
done
#################################################################################################################