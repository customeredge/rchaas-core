#################################################################################################################
export CLOUDS="fe vdr"
export ENVS="di:qual pr:prod"
export ZONES="exec admin"

for CLOUD in ${CLOUDS}; do
    for ENV_FULL in ${ENVS}; do
        ENV=${ENV_FULL%:*}
        PF_ENV=${ENV_FULL#*:}
   
        source ./1-get_vault_token.sh

        echo ""
        echo "CLOUD: ${CLOUD}"
        echo "ENV: ${ENV}"        
        echo "vault_kv_path: ${vault_kv_path}"

        for ZONE in ${ZONES};do
            # OPENSTACK
            vault kv put -tls-skip-verify ${vault_kv_path}/openstack/${ZONE}     @data/${CLOUD}/openstack/${ENV}/${ZONE}/${PF_ENV}-diod-k8s-openstack.json 1>/dev/null && \
            echo Success! Data written to: ${vault_kv_path}/openstack/${ZONE}
        done
        
        # DOCKER
        vault kv put -tls-skip-verify ${vault_kv_path}/docker                @data/${CLOUD}/docker/${ENV}/${PF_ENV}-diod-k8s-docker.json 1>/dev/null && \
        echo Success! Data written to: ${vault_kv_path}/docker

        # CONSUL
        vault kv put -tls-skip-verify ${vault_kv_path}/consul                @data/${CLOUD}/consul/${ENV}/${PF_ENV}-diod-k8s-consul.json 1>/dev/null && \
        echo Success! Data written to: ${vault_kv_path}/consul

        # S3
        vault kv put -tls-skip-verify ${vault_kv_path}/s3                    @data/${CLOUD}/s3/${ENV}/${PF_ENV}-diod-k8s-s3.json 1>/dev/null && \
        echo Success! Data written to: ${vault_kv_path}/s3

        # POWERDNS
        vault kv put -tls-skip-verify ${vault_kv_path}/powerdns              @data/${CLOUD}/powerdns/${ENV}/${PF_ENV}-diod-k8s-powerdns.json 1>/dev/null && \
        echo Success! Data written to: ${vault_kv_path}/powerdns

        # RANCHER
        vault kv put -tls-skip-verify ${vault_kv_path}/rancher               @data/${CLOUD}/rancher/${ENV}/${PF_ENV}-diod-k8s-rancher.json 1>/dev/null && \
        echo Success! Data written to: ${vault_kv_path}/rancher

        # KEYCLOAK
        vault kv put -tls-skip-verify ${vault_kv_path}/keycloak             @data/${CLOUD}/keycloak/${ENV}/${PF_ENV}-diod-k8s-keycloak.json 1>/dev/null && \
        echo Success! Data written to: ${vault_kv_path}/keycloak

        # VMC
        vault kv put -tls-skip-verify ${vault_kv_path}/vmc                   @data/${CLOUD}/vmc/${ENV}/${PF_ENV}-diod-k8s-vmc.json 1>/dev/null && \
        echo Success! Data written to: ${vault_kv_path}/vmc

    done
done
#################################################################################################################