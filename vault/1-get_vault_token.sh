export vault_kv_path=secret/products/diod-${CLOUD}/diod-k8s/${PF_ENV}/rchaas

source .credentials/.vault_${CLOUD}_$ENV

export VAULT_TOKEN=$(vault write -tls-skip-verify auth/approle/login \
      role_id=$ROLE_ID \
      secret_id=$SECRET_ID | grep token | grep -v token_ | awk '{ print $2}')
